// Shaky top-level build script

plugins {
    kotlin("jvm") version "1.9.10"
    id("org.jetbrains.dokka") version "1.8.10"
    application
}

allprojects {
    apply(plugin = "org.jetbrains.kotlin.jvm")

    repositories {
        mavenCentral()
        // Glok is a GUI toolkit similar to JavaFX, but with low level access to OpenGL.
        maven {
            name = "glok"
            url = uri("https://gitlab.com/api/v4/projects/46354938/packages/maven")
        }
    }
}

val shakyVersion: String by project
val glokVersion: String by project

application {
    mainClass.set("uk.co.nickthecoder.shaky.Shaky")
}

dependencies {
    implementation(project(":shaky-core"))
    implementation(project(":shaky-editor"))

    implementation("uk.co.nickthecoder:glok-model:$glokVersion")
    implementation("uk.co.nickthecoder:glok-core:$glokVersion")
}

task<Exec>("publishDokka") {
    dependsOn(":dokkaHtmlMultiModule")
    commandLine("./publishDokka.feathers", "build/dokka/htmlMultiModule/", "../api/vectorial-$shakyVersion")
}

task<Exec>("publishZip") {
    dependsOn(":distZip")
    commandLine("cp", "build/distributions/shaky-${shakyVersion}.zip", "../download/")
}

task<Exec>("ntc") {
    dependsOn(":publishDokka", ":publishZip")
    commandLine("echo", "Done")
}

defaultTasks("installDist")
