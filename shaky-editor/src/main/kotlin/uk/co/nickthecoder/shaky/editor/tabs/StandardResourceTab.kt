package uk.co.nickthecoder.shaky.editor.tabs

import uk.co.nickthecoder.glok.collections.sizeProperty
import uk.co.nickthecoder.glok.control.ButtonMeaning
import uk.co.nickthecoder.glok.property.boilerplate.optionalNodeProperty
import uk.co.nickthecoder.glok.property.functions.greaterThan
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.shaky.core.resources.Resource
import uk.co.nickthecoder.shaky.editor.controls.heading

abstract class StandardResourceTab<R : Resource>(resource: R) : ResourceTab<R>(resource) {

    private val detailsProperty by optionalNodeProperty(null)
    protected var details by detailsProperty

    protected val tabPane = tabPane {
        // If there is only the "Details" tab, then we don't need to see the tabBar.
        tabBar.visibleProperty.bindTo(tabs.sizeProperty().greaterThan(1))
    }

    init {
        textProperty.bindTo(resource.nameProperty)
        content = borderPane {
            top = heading(title()) {}
            center = tabPane.apply {
                + tab("Details") { contentProperty.bindTo(detailsProperty) }
            }
        }
    }

    open fun title() = resource.resourceType.title

    override fun focusOnContent() {
        details?.requestFocus(true)
    }
}
