package uk.co.nickthecoder.shaky.editor

import uk.co.nickthecoder.glok.property.boilerplate.intProperty

object EditorSettings : AbstractSettings(Editor.preferences()) {

    val iconSizeProperty by intProperty(24)
    var iconSize by iconSizeProperty

}
