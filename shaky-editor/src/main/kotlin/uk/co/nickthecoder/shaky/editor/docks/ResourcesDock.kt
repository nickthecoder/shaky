package uk.co.nickthecoder.shaky.editor.docks

import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.dock.Dock
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.dsl.menuItem
import uk.co.nickthecoder.shaky.core.resources.*
import uk.co.nickthecoder.shaky.editor.Editor
import uk.co.nickthecoder.shaky.editor.EditorCommands
import uk.co.nickthecoder.shaky.editor.EditorState
import uk.co.nickthecoder.shaky.editor.controls.newResource
import uk.co.nickthecoder.shaky.editor.openInExternalApp
import java.lang.IllegalStateException

class ResourcesDock(harbour: Harbour, val commands: EditorCommands, val state: EditorState) : Dock(ID, harbour) {

    private val treeView = MixedTreeView<AnyItem>().apply {
        root = RootItem()
        showRoot = true
    }

    init {
        title = "Resources"
        allowedSides.remove(Side.TOP)
        allowedSides.remove(Side.BOTTOM)
        content = treeView
    }

    companion object {
        const val ID = "RESOURCES"
    }

    private abstract inner class AnyItem : MixedTreeItem<AnyItem>() {
        abstract val name: String
        abstract val resourceType: ResourceType

        override fun createCell(treeView: MixedTreeView<AnyItem>): MixedTreeCell<AnyItem> {
            return TextMixedTreeCell(treeView, this, name).apply {
                node.graphic = ImageView(Editor.resizableIcons[resourceType.iconName()])
                onMouseClicked { event ->
                    if (event.clickCount == 2) {
                        open()
                    }
                }
                onPopupTrigger {
                    val menu = PopupMenu()
                    menu.contextMenu()
                    if (menu.items.isNotEmpty()) {
                        menu.show(this, side.opposite())
                    }
                }
            }
        }

        protected fun addOrdered(item: AnyItem) {
            val itemName = item.name
            val resourceType = item.resourceType
            for ((index, child) in children.withIndex()) {
                val rtComparison = resourceType.compareTo(child.resourceType)
                if (rtComparison < 0) {
                    children.add(index, item)
                    return
                } else if (rtComparison == 0 && itemName > child.name) {
                    children.add(index, item)
                    return
                }
            }
            children.add(item)
        }

        open fun PopupMenu.contextMenu() {}
        open fun open() {}
    }

    private abstract inner class ResourceItem<R : Resource>(
        val resource: R
    ) : AnyItem() {

        override val name get() = resource.name
        override val resourceType get() = resource.resourceType

        init {
            leaf = true
        }

        override fun open() {
            commands.openResource(resource)
        }

        override fun PopupMenu.contextMenu() {
            + menuItem("Delete") {
                onAction {
                    resource.parentResource.children.remove(resource.name)
                }
            }
        }
    }


    private abstract inner class ParentResourceItem(
        val resource: ParentResource
    ) : AnyItem() {

        init {

            for (resource in resource.children.values.sorted()) {
                children.add(createChild(resource))
            }
            resource.children.addChangeListener { _, changes ->
                for (change in changes) {
                    if (change.isRemoval) {
                        children.removeIf { it is ResourceItem<*> && it.resource === change.value }
                        children.removeIf { it is ParentResourceItem && it.resource === change.value }
                    } else {
                        if (change.oldValue != null) children.removeIf { it.name == change.key }
                        addOrdered(createChild(change.value))
                    }
                }
            }
        }

        fun createChild(resource: Resource): AnyItem {
            return when (resource) {
                is TextureResource -> TextureItem(resource)
                is InputResource -> InputItem(resource)
                is FolderResource -> FolderItem(resource)
                else -> throw IllegalStateException("Unexpected resource class : ${resource.javaClass.name}")
            }
        }

        override fun PopupMenu.contextMenu() {
            + menuItem("Open Folder") {
                onAction { Editor.directory(resource).openInExternalApp() }
            }

            + Separator()

            + menuItem("New Texture") {
                onAction { newResource(commands, state, ResourceType.TEXTURE, resource) }
            }
            + menuItem("New Input") {
                onAction { newResource(commands, state, ResourceType.INPUT, resource) }
            }
            + menuItem("New Folder") {
                onAction { newResource(commands, state, ResourceType.FOLDER, resource) }
            }
            + Separator()

            if (resource is Resource) {
                + menuItem("Delete") {
                    onAction {
                        resource.parentResource.children.remove(resource.name)
                    }
                }
            }

        }
    }

    private inner class RootItem : ParentResourceItem(state.resources) {
        override val name get() = state.resources.gameInfo.title
        override val resourceType get() = ResourceType.GAME

        init {
            children.add(0, GameInfoItem())
            expanded = true
        }
    }

    private inner class FolderItem(val folderResource: FolderResource) : ParentResourceItem(folderResource) {
        override val name: String get() = folderResource.name
        override val resourceType get() = ResourceType.FOLDER
    }

    private inner class GameInfoItem : ResourceItem<GameInfo>(state.resources.gameInfo)

    private inner class TextureItem(texture: TextureResource) : ResourceItem<TextureResource>(texture)

    private inner class InputItem(resource: InputResource) : ResourceItem<InputResource>(resource)

}
