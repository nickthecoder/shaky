package uk.co.nickthecoder.shaky.editor.tabs

import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.property.SimpleProperty
import uk.co.nickthecoder.glok.property.UnaryFunction
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.shaky.core.resources.ScatteredPose
import uk.co.nickthecoder.shaky.core.resources.ScatteredTextureResource
import uk.co.nickthecoder.shaky.editor.Editor
import uk.co.nickthecoder.shaky.editor.controls.filenameRow
import uk.co.nickthecoder.shaky.editor.controls.resourceNameRow
import uk.co.nickthecoder.shaky.editor.controls.shakyForm
import uk.co.nickthecoder.shaky.editor.controls.xAndY

class ScatteredTextureTab(textureResource: ScatteredTextureResource) :
    TextureTab<ScatteredTextureResource>(textureResource) {

    private val poseProperty = SimpleProperty<ScatteredPose?>(null)
    private var pose by poseProperty

    init {
        details = vBox {
            fillWidth = true
            growPriority = 1f

            + shakyForm {
                with(textureResource) {
                    filenameRow(Editor.directory(resource.parentResource), nameProperty)
                }
            }
            + Separator()
            + scatteredExtras()

        }
    }

    private fun scatteredExtras(): Node {

        return splitPane {
            growPriority = 1f
            + singleContainer {
                contentProperty.bindTo(UnaryFunction(poseProperty) { pose -> scatteredPoseDetails(pose) })
            }
            + stackPane {
                padding(10, 0, 0, 10)
                alignment = Alignment.TOP_LEFT
                + imageView(resource.texture)
                + HighlightPose().apply {
                    this.poseProperty.bindCastTo(this@ScatteredTextureTab.poseProperty)
                }
                + SelectScatteredPose().apply {
                    this.poseProperty.bidirectionalBind(this@ScatteredTextureTab.poseProperty)
                }
            }
        }
    }

    private fun scatteredPoseDetails(pose: ScatteredPose?): Node {

        return if (pose == null) {
            label("No Pose selected") {
                padding(6)
                alignment = Alignment.TOP_LEFT
            }
        } else {

            shakyForm {
                resourceNameRow(pose, label = "Pose Name")
                row("Position") {
                    right = xAndY(pose.leftProperty, pose.topProperty)
                }
                row("Size") {
                    right = xAndY(pose.widthProperty, pose.heightProperty)
                }
                row("Offset") {
                    right = xAndY(pose.xOffsetProperty, pose.yOffsetProperty)
                }
                row {
                    right = button("Delete") {
                        onAction {
                            resource.children.remove(pose.name)
                            poseProperty.value = null
                        }
                    }
                }
                row {
                    below = ImageView(pose.imageProperty)
                }
            }
        }
    }

}
