package uk.co.nickthecoder.shaky.editor

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.scene.dsl.menuBar

fun mainMenuBar(commands: Commands, state: EditorState) = menuBar {
    with(commands) {
        with(EditorActions) {
            + menu(FILE) {
                + menuItem(FILE_SAVE)
                //+ subMenu(FILE_NEW_RESOURCE) {
                //    + menuItem(FILE_NEW_TEXTURE)
                //    + menuItem(FILE_NEW_INPUT)
                //}
                + Separator()
                + menuItem(FILE_CLOSE_TAB)
                + menuItem(FILE_EXIT)
            }

            + menu(EDIT) {
            }

            + menu(VIEW) {
                + checkMenuItem(VIEW_RESOURCES)
                + checkMenuItem(VIEW_NODE_INSPECTOR)
            }
        }
    }
}
