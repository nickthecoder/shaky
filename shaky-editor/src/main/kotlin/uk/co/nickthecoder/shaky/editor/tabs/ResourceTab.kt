package uk.co.nickthecoder.shaky.editor.tabs

import uk.co.nickthecoder.glok.control.Tab
import uk.co.nickthecoder.shaky.core.resources.*
import uk.co.nickthecoder.shaky.editor.controls.*

abstract class ResourceTab<R : Resource>(val resource: R) : Tab(resource.name) {

    abstract fun focusOnContent()

    init {
        graphic = resource.resourceType.graphic()
    }

    companion object {
        fun create(resource: Resource): ResourceTab<*>? =
            when (resource) {
                is GameInfo -> GameInfoTab(resource)

                is SingleTextureResource -> SingleTextureTab(resource)
                is GridTextureResource -> GridTextureTab(resource)
                is ScatteredTextureResource -> ScatteredTextureTab(resource)

                is InputResource -> InputTab(resource)

                else -> null
            }
    }
}
