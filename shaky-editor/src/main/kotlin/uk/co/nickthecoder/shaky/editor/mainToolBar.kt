package uk.co.nickthecoder.shaky.editor

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.scene.dsl.toolBar

fun mainToolBar(commands: Commands, state: EditorState) = toolBar {
    + label("ToolBar")
}
