package uk.co.nickthecoder.shaky.editor.tabs

import uk.co.nickthecoder.glok.control.FormGrid
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.dsl.imageView
import uk.co.nickthecoder.glok.scene.dsl.row
import uk.co.nickthecoder.glok.scene.dsl.stackPane
import uk.co.nickthecoder.glok.scene.dsl.vBox
import uk.co.nickthecoder.shaky.core.resources.SingleTextureResource
import uk.co.nickthecoder.shaky.editor.Editor
import uk.co.nickthecoder.shaky.editor.controls.filenameRow
import uk.co.nickthecoder.shaky.editor.controls.shakyForm
import uk.co.nickthecoder.shaky.editor.controls.xAndY

class SingleTextureTab(textureResource: SingleTextureResource) :
    TextureTab<SingleTextureResource>(textureResource) {

    init {
        details = vBox {
            fillWidth = true
            growPriority = 1f

            + shakyForm {
                with(textureResource) {
                    filenameRow(Editor.directory(resource.parentResource), nameProperty)

                    singleTextureRows(resource)
                }
            }
        }
    }

    private fun FormGrid.singleTextureRows(single: SingleTextureResource) {

        row("Offset") {
            right = xAndY(single.xOffsetProperty, single.yOffsetProperty)
        }

        row {
            above = stackPane {
                + imageView(resource.texture)
                + HighlightPose().apply {
                    pose = single
                    highlightColor = Color.TRANSPARENT
                }
            }
        }

    }

}
