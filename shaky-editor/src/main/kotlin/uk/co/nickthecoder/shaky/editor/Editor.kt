package uk.co.nickthecoder.shaky.editor

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.row
import uk.co.nickthecoder.glok.scene.dsl.texture
import uk.co.nickthecoder.glok.scene.icons
import uk.co.nickthecoder.glok.util.Log
import uk.co.nickthecoder.glok.util.log
import uk.co.nickthecoder.shaky.core.resources.ParentResource
import java.io.File
import java.util.prefs.Preferences

class Editor : Application() {

    override fun start(primaryStage: Stage) {
        log.level = Log.INFO
        shakyFile = File(rawArguments.first())
        primaryStage.title = "Shaky Editor"
        MainWindow(primaryStage, shakyFile)
        primaryStage.show()
    }

    companion object {

        val icons by lazy {
            icons(backend.resources("uk/co/nickthecoder/shaky/editor")) {
                texture("editorIcons.png", 64) {
                    row(
                        "file_", 2, 2, 4,
                        "new", "open", "save", "save_as", "properties"
                    )
                    row(
                        2, 70, 4,
                        "edit_copy", "edit_paste", "edit_paste", "edit_delete",
                        "edit_undo", "edit_redo", "select_all", "select_none", "find", "replace"
                    )
                    row(
                        2, 2 + 68 * 2, 4,
                        "to_bottom", "lower", "raise", "to_top", "remove", "add",
                        "blank", "blank", "model_extension", "shape_extension"
                    )
                    row(
                        "resource_", 2, 2 + 68 * 3, 4,
                        "game", "game_info", "texture", "input", "folder"
                    )
                    row(
                        2, 2 + 68 * 4, 4,
                        "input_key", "input_mouse", "input_game_pad"
                    )

                    // 1 blank rows

                    row(
                        2, 2 + 68 * 6, 4, "refresh", "settings_general", "debug", "sync", "clear",
                        "close", "settings_slicer", "customiser"
                    )
                }
            }
        }

        val resizableIcons by lazy { icons.resizableIcons(EditorSettings.iconSizeProperty) }

        internal lateinit var shakyFile: File

        /**
         * The root node, where [EditorSettings] are saved. [Harbour]'s state is saved is a child node.
         */
        fun preferences(): Preferences = Preferences.userNodeForPackage(Editor::class.java)

        fun directory(parentResource: ParentResource) = File(shakyFile.parentFile, parentResource.path)

        /**
         * This is not the usual entry point, but can be useful to launch the editor directly from your IDE.
         * The usual entry point is in class `Shaky`, in the top-level project.
         */
        @JvmStatic
        fun main(vararg args: String) {
            launch(Editor::class, args)
        }
    }

}
