package uk.co.nickthecoder.shaky.editor.tabs

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.shaky.core.resources.*
import uk.co.nickthecoder.shaky.editor.clamp
import kotlin.math.abs
import kotlin.math.min

abstract class TextureTab<T : TextureResource>(textureResource: T) : StandardResourceTab<T>(textureResource) {

    override fun title(): String {
        return "${super.title()} ( ${resource.textureType.title} )"
    }

    // region ==== HighlightPose ====
    inner class HighlightPose : Node() {

        val poseProperty by property<Pose?>(null)
        var pose by poseProperty

        val highlightColorProperty by stylableColorProperty(Color.BLACK)
        var highlightColor by highlightColorProperty

        val crossHairsColorProperty by stylableColorProperty(Color.WHITE)
        var crossHairsColor by crossHairsColorProperty

        init {
            style("highlight_pose")
        }

        override fun draw() {

            val pose = pose ?: return

            // Highlight selected pose
            backend.fillRect(
                sceneX + pose.left, sceneY + pose.top,
                sceneX + pose.left + pose.width, sceneY + pose.top + pose.height,
                highlightColor
            )
            // Grid

            // Cross-hairs
            backend.fillRect(
                sceneX + pose.left + pose.xOffset,
                sceneY + pose.top,
                sceneX + pose.left + pose.xOffset + 1,
                sceneY + pose.top + pose.height,
                crossHairsColor
            )
            backend.fillRect(
                sceneX + pose.left,
                sceneY + pose.top + pose.yOffset,
                sceneX + pose.left + pose.width,
                sceneY + pose.top + pose.yOffset + 1,
                crossHairsColor
            )
        }

        override fun nodePrefWidth() = resource.texture.imageWidth

        override fun nodePrefHeight() = resource.texture.imageHeight

    }
    // endregion HighlightPose

    // region ==== SelectScatteredPose ====
    inner class SelectScatteredPose : Node() {

        val poseProperty by property<ScatteredPose?>(null)
        var pose by poseProperty

        val highlightColorProperty by stylableColorProperty(Color.BLACK)
        var highlightColor by highlightColorProperty

        private var xStart = - 1
        private var yStart = - 1
        private var xEnd = - 1
        private var yEnd = - 1

        init {
            style("select_scattered_pose")

            // We are adding the event handlers to the parent, so that we can drag from
            // the padding of the parent, without having the deal with the hassle of
            // adding padding to each part of the parent StackPane.
            parentProperty.addChangeListener { _, _, parent ->
                if (parent != null) {
                    parent.onMouseClicked { event ->
                        val x = event.poseX()
                        val y = event.poseY()
                        pose = resource.children.values.filterIsInstance<ScatteredPose>().firstOrNull {
                            it.left <= x && it.left + it.width >= x &&
                                it.top <= y && it.top + it.height >= y
                        }
                    }

                    parent.onMousePressed { event ->
                        if (event.isPrimary) {
                            if (event.isControlDown) {
                                // Start dragging to select the area of a new Pose
                                event.capture()
                                xStart = event.poseX()
                                yStart = event.poseY()
                                event.consume()
                            }
                        }
                    }

                    parent.onMouseDragged { event ->
                        xEnd = event.poseX()
                        yEnd = event.poseY()
                    }

                    parent.onMouseReleased { event ->
                        if (xEnd > 0) {
                            val xDelta = abs(xStart - xEnd)
                            val yDelta = abs(yStart - yEnd)
                            val newPose = ScatteredPose(resource).apply {
                                name = resource.unusedName("New")
                                left = min(xStart, xEnd)
                                top = min(yStart, yEnd)
                                width = xDelta
                                height = yDelta
                            }
                            // Do NOT add this to the parent yet.
                            // It will be added when the name is changed by the user.
                            // resource.children[newPose.name] = newPose
                            poseProperty.value = newPose
                        }
                        xStart = - 1
                        yStart = - 1
                        xEnd = - 1
                        yEnd = - 1
                    }
                }
            }
        }

        private fun MouseEvent.poseX() = (sceneX - this@SelectScatteredPose.sceneX).toInt()
            .clamp(0, resource.texture.width - 1)

        private fun MouseEvent.poseY() = (sceneY - this@SelectScatteredPose.sceneY).toInt()
            .clamp(0, resource.texture.height - 1)

        override fun draw() {
            if (xStart > 0) {
                backend.fillRect(
                    sceneX + xStart, sceneY + yStart,
                    sceneX + xEnd, sceneY + yEnd,
                    highlightColor
                )
            }
        }

        override fun nodePrefWidth() = resource.texture.imageWidth

        override fun nodePrefHeight() = resource.texture.imageHeight

    }
    // endregion SelectScatteredPose
}

