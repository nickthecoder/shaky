package uk.co.nickthecoder.shaky.editor

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.event.Key

object EditorActions : Actions() {

    val FILE = define("file", "File")
    val FILE_SAVE = define("file_save", "Save", Key.S.control())
    val FILE_EXIT = define("file_exit", "Exit", Key.Q.control())
    val FILE_CLOSE_TAB = define("file_close_tab", "Close Tab", Key.W.control())


    val EDIT = define("edit", "Edit")

    val VIEW = define("view", "View")
    val VIEW_RESOURCES = define("view_resources", "Resources", Key.DIGIT_1.alt())
    val VIEW_NODE_INSPECTOR = define("view_node_inspector", "Node Inspector", Key.DIGIT_9.alt())

}
