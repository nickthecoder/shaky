package uk.co.nickthecoder.shaky.editor.controls

import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.property.boilerplate.StringProperty
import uk.co.nickthecoder.glok.property.boilerplate.StringUnaryFunction
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.dsl.button
import uk.co.nickthecoder.glok.scene.dsl.hBox
import uk.co.nickthecoder.glok.scene.dsl.row
import uk.co.nickthecoder.glok.scene.dsl.textField
import uk.co.nickthecoder.glok.util.Converter
import uk.co.nickthecoder.shaky.core.resources.ParentResource
import uk.co.nickthecoder.shaky.core.resources.Resource
import uk.co.nickthecoder.shaky.editor.Editor
import uk.co.nickthecoder.shaky.editor.containsAnyOf
import java.io.File

fun FormGrid.resourceNameRow(resource: Resource, required: Boolean = true, label: String = "Name") {

    val textField = textField {
        prefColumnCount = 20
        textProperty.bidirectionalBind(resource.nameProperty)
    }

    row(label) {
        right = textField
        errors = errorMessage {
            textProperty.bindTo(StringUnaryFunction(textField.textProperty) { text ->
                if (text.isEmpty() && required) {
                    "Required"
                } else {
                    val existing = resource.parentResource.children[text]
                    if (existing != null && existing !== resource) {
                        "Name already used"
                    } else {
                        ""
                    }
                }
            })
        }
    }
}

fun FormGrid.newResourceNameRow(oldName: String, parentResource: ParentResource): TextField {

    val textField = textField {
        prefColumnCount = 20
        text = oldName
    }

    val errorMessage = errorMessage {
        textProperty.bindTo(StringUnaryFunction(textField.textProperty) { text ->
            if (text.isEmpty()) {
                "Required"
            } else {
                val existing = parentResource.children[text]
                if (existing != null) {
                    "Name already used"
                } else {
                    ""
                }
            }
        })
    }

    row("Name") {
        right = textField
        below = errorMessage
    }

    return textField
}

// TODO Check the folder name isn't used as a resource name.
fun FormGrid.selectFilenameRow(directory: File): TextField {

    val textField = textField {
        prefColumnCount = 20
    }

    val errorMessage = errorMessage {
        textProperty.bindTo(StringUnaryFunction(textField.textProperty) { text ->
            if (text.isEmpty()) {
                "Required"
            } else {
                val file = File(directory, text)
                if (file.exists()) {
                    ""
                } else {
                    "File not found"
                }
            }
        })
    }

    row("Filename") {
        right = hBox {
            alignment = Alignment.CENTER_LEFT

            + textField
            + button("…").apply {
                contentDisplay = ContentDisplay.GRAPHIC_ONLY
                graphic = ImageView(Editor.resizableIcons["file_open"])
                onAction {
                    FileDialog().apply {
                        initialDirectory = directory

                        extensions.addAll(ExtensionFilter("Images", "*.png", "*.jpeg", "*.jgp"))
                        extensions.addAll(ExtensionFilter("All Files", "*"))

                        showOpenDialog(scene !!.stage !!) { file ->
                            if (file != null) {
                                if (file.parentFile.absoluteFile == directory.absoluteFile) {
                                    textField.text = file.name
                                } else {
                                    println("You can only choose a file in directory : $directory")
                                }
                            }
                        }
                    }
                }
            }
        }
        below = errorMessage

    }

    return textField
}

// TODO Check folder name isn't used as a child resource name.
fun FormGrid.selectFolderRow(directory: File): TextField {

    val textField = textField {
        prefColumnCount = 20
    }

    val errorMessage = errorMessage {
        textProperty.bindTo(StringUnaryFunction(textField.textProperty) { text ->
            if (text.isEmpty()) {
                "Required"
            } else {
                val file = File(directory, text)
                if (file.exists() && file.isFile) {
                    "This is a file, not a folder"
                } else {
                    ""
                }
            }
        })
    }

    row("Folder") {
        right = hBox {
            alignment = Alignment.CENTER_LEFT

            + textField
            + button("…").apply {
                contentDisplay = ContentDisplay.GRAPHIC_ONLY
                graphic = ImageView(Editor.resizableIcons["file_open"])
                onAction {
                    FileDialog().apply {
                        initialDirectory = directory

                        showFolderPicker(scene !!.stage !!) { file ->
                            if (file != null) {
                                if (file.parentFile.absoluteFile == directory.absoluteFile) {
                                    textField.text = file.name
                                } else {
                                    println("You can only choose a sub folder of directory : $directory")
                                }
                            }
                        }
                    }
                }
            }
        }
        below = errorMessage

    }

    return textField
}

/**
 *
 */
fun FormGrid.filenameRow(directory: File, filenameProperty: StringProperty) {

    var oldText = filenameProperty.value

    val textField = textField(oldText) {
        prefColumnCount = 20
    }
    val errorMessage = errorMessage()

    row("Filename") {
        right = textField
        below = errorMessage
    }

    textField.textProperty.addChangeListener { _, _, text ->
        if (text.isEmpty()) {
            errorMessage.text = "Required"
        } else if (text.containsAnyOf(",", "/", "\\", ":", "..")) {
            errorMessage.text = "Special characters are not allowed ( / , \\ , : , ; , .. )"
        } else {
            val newFile = File(directory, text)
            if (text != oldText && newFile.exists()) {
                errorMessage.text = "File exists"
            } else {
                errorMessage.text = ""
            }
        }
    }

    textField.focusedProperty.addChangeListener { _, _, focus ->
        if (! focus && ! errorMessage.visible) {
            val newFile = File(directory, textField.text)
            File(directory, oldText).renameTo(newFile)
            oldText = textField.text
            filenameProperty.value = oldText
        }
    }
}

