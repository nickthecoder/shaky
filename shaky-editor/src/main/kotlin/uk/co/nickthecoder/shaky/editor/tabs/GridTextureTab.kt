package uk.co.nickthecoder.shaky.editor.tabs

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.property.BinaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.OptionalNodeTernaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.SimpleIntProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableColorProperty
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.shaky.core.resources.GridPose
import uk.co.nickthecoder.shaky.core.resources.GridTextureResource
import uk.co.nickthecoder.shaky.editor.Editor
import uk.co.nickthecoder.shaky.editor.clamp
import uk.co.nickthecoder.shaky.editor.controls.*

class GridTextureTab(textureResource: GridTextureResource) : TextureTab<GridTextureResource>(textureResource) {

    private val xProperty = SimpleIntProperty(0)
    private val yProperty = SimpleIntProperty(0)
    private val gridPoseProperty = BinaryFunction(xProperty, yProperty) { x, y ->
        if (x >= 0 && y >= 0) {
            resource.findPose(x, y) ?: (GridPose(resource, x, y).apply { resource.addPose(this) })
        } else {
            null
        }
    }

    init {
        details = vBox {
            fillWidth = true
            growPriority = 1f

            + shakyForm {
                with(textureResource) {
                    filenameRow(Editor.directory(resource.parentResource), nameProperty)
                }
            }
            tabPane.tabs.add(gridDetails())
            if (resource.children.isEmpty()) {
                tabPane.selection.selectedIndex = 1
            }

            + Separator()
            + gridPoses()

        }
    }

    private fun gridDetails() = tab("Grid Attributes") {
        content = shakyForm {
            row("Pose Size") {
                right = xAndY(resource.poseWidthProperty, resource.poseHeightProperty)
            }

            row("Padding") {
                above = information("Include at least 1 pixel of padding and spacing to prevent bleed-through")
                right = xAndY(resource.xPaddingProperty, resource.yPaddingProperty)
            }

            row("Spacing") {
                right = xAndY(resource.xSpacingProperty, resource.ySpacingProperty)
            }

            row("Shared Offsets?") {
                right = horizontalFields {
                    + checkBox {
                        selectedProperty.bidirectionalBind(resource.sharedOffsetsProperty)
                    }
                    + xAndY(resource.sharedXOffsetProperty, resource.sharedYOffsetProperty).apply {
                        visibleProperty.bindTo(resource.sharedOffsetsProperty)
                    }
                }
                below = information("When checked, all Poses will have identical offsets")
            }

            row {
                above = stackPane {
                    + imageView(this@GridTextureTab.resource.texture)
                    + Grid()
                }
            }
        }
    }

    private fun gridPoses() = splitPane {
        growPriority = 1f

        + vBox {
            fillWidth = true

            + shakyForm {
                row("Grid Position") {
                    right = xAndY(xProperty, yProperty, maxX = resource.across - 1, maxY = resource.down - 1)
                }
            }

            + Separator()

            + singleContainer {
                contentProperty.bindTo(
                    OptionalNodeTernaryFunction(xProperty, yProperty, gridPoseProperty) { _, _, _ ->
                        gridPoseDetails()
                    }
                )
            }
        }

        + stackPane {
            alignment = Alignment.TOP_LEFT
            padding(10)
            growPriority = 1f

            + imageView(this@GridTextureTab.resource.texture)

            + HighlightPose().apply {
                this.poseProperty.bindCastTo(gridPoseProperty)
            }

            + Grid().apply {
                tooltip = TextTooltip("Click to select a pose")
            }
        }
    }

    private fun gridPoseDetails() = shakyForm {

        val pose = gridPoseProperty.value
        val x = xProperty.value
        val y = yProperty.value

        if (pose != null) {
            resourceNameRow(pose, required = false)

            row("Offset") {
                right =
                    xAndY(pose.xOffsetProperty, pose.yOffsetProperty, readOnlyProperty = resource.sharedOffsetsProperty)
            }

            row("") {
                right = button("Delete") {
                    onAction {
                        resource.remove(x, y)
                        // Forces the details to be reevaluated.
                        xProperty.value = - 1
                        xProperty.value = x
                    }
                }
            }
            row {
                below = ImageView(pose.imageProperty)
            }
        }
    }

    // region ==== Grid ====
    inner class Grid : Node() {

        val gridColorProperty by stylableColorProperty(Color.BLACK)
        val gridColor by gridColorProperty

        init {
            style("grid")

            onMouseClicked { event ->
                if (event.isPrimary) {
                    xProperty.value = (
                        (
                            event.sceneX - sceneX - resource.xPadding) /
                            (resource.poseWidth + resource.xSpacing)
                        )
                        .toInt()
                        .clamp(0, resource.across - 1)
                    yProperty.value = (
                        (
                            event.sceneY - sceneY - resource.yPadding) /
                            (resource.poseHeight + resource.ySpacing)
                        )
                        .toInt()
                        .clamp(0, resource.down - 1)
                }
            }
        }

        override fun draw() {

            with(resource) {
                val maxX = sceneX + texture.width - poseWidth
                val maxY = sceneY + texture.height - poseHeight

                var y = sceneY + yPadding.toFloat()
                var yCount = 0

                while (y <= maxY && yCount < 50) {
                    var x = sceneX + xPadding.toFloat()
                    var xCount = 0
                    while (x <= maxX && xCount < 50) {

                        backend.strokeInsideRect(
                            x - 1, y - 1,
                            x + poseWidth + 1, y + poseHeight + 1,
                            1f,
                            gridColor
                        )

                        x += poseWidth + xSpacing
                        xCount ++
                    }

                    y += poseHeight + ySpacing
                    yCount ++
                }
            }
        }

        override fun nodePrefWidth() = resource.texture.imageWidth

        override fun nodePrefHeight() = resource.texture.imageHeight

    }
    // endregion Grid

}
