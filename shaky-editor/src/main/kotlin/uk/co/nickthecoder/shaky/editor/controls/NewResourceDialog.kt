package uk.co.nickthecoder.shaky.editor.controls

import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.dialog.Dialog
import uk.co.nickthecoder.glok.property.SimpleProperty
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty
import uk.co.nickthecoder.glok.scene.StageType
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.util.Converter
import uk.co.nickthecoder.shaky.core.resources.*
import uk.co.nickthecoder.shaky.editor.*

fun newResource(commands: EditorCommands, state: EditorState, resourceType: ResourceType, parent: ParentResource) {
    when (resourceType) {
        ResourceType.FOLDER -> NewFolderDialog(commands, state, parent).show()
        ResourceType.TEXTURE -> NewTextureDialog(commands, state, parent).show()
        ResourceType.INPUT -> NewInputDialog(commands, state, parent).show()
        else -> {}
    }
}

abstract class NewResourceDialog<R : Resource>(
    private val commands: EditorCommands,
    private val state: EditorState,
    private val resourceType: ResourceType
) : Dialog() {

    private var canClose = false

    private val formGrid = shakyForm {}

    protected val resourceNameProperty by stringProperty("")
    protected var resourceName by resourceNameProperty

    init {
        title = "New ${resourceType.title}"
        buttonTypes(ButtonType.OK, ButtonType.CANCEL) { reply ->
            if (reply == ButtonType.OK) {
                if (formGrid.isValid()) {
                    val resource = createResource()
                    commands.openResource(resource)
                    canClose = true
                    println("Can close")
                }
            } else {
                canClose = true
            }
        }

        content = vBox {
            fillWidth = true
            + heading(title) {
                graphic = resourceType.graphic()
            }
            + formGrid
        }

    }

    fun show() {
        formGrid.addRows()
        createStage(state.glokStage, StageType.MODAL) {
            onCloseRequested { event ->
                if (! canClose) event.consume()
            }
            show()
        }
    }

    abstract fun createResource(): Resource
    open fun FormGrid.addRows() {}
}

class NewFolderDialog(commands: EditorCommands, state: EditorState, private val parentResource: ParentResource) :
    NewResourceDialog<FolderResource>(commands, state, ResourceType.FOLDER) {

    override fun createResource(): FolderResource {
        return FolderResource(parentResource).apply {
            name = resourceName
            parentResource.children[name] = this
            val dir = Editor.directory(this)
            if (! dir.exists()) {
                dir.mkdir()
            }
        }
    }

    override fun FormGrid.addRows() {
        selectFolderRow(Editor.directory(parentResource))
            .textProperty.bidirectionalBind(resourceNameProperty)
    }
}

class NewTextureDialog(commands: EditorCommands, state: EditorState, private val parentResource: ParentResource) :
    NewResourceDialog<TextureResource>(commands, state, ResourceType.TEXTURE) {

    val textureTypeProperty = SimpleProperty<TextureType?>(TextureType.SINGLE)

    override fun createResource(): TextureResource {
        return TextureResource.create(textureTypeProperty.value ?: TextureType.SINGLE, parentResource).apply {
            name = resourceName
            parentResource.children[name] = this
        }
    }

    override fun FormGrid.addRows() {
        selectFilenameRow(Editor.directory(parentResource))
            .textProperty.bidirectionalBind(resourceNameProperty)
        row("Type") {
            right = choiceBox<TextureType> {
                converter = { it?.title ?: "" }
                items.addAll(TextureType.values())
                selection.selectedItem = TextureType.SINGLE
                textureTypeProperty.bindTo(selection.selectedItemProperty)
            }
        }
    }
}

class NewInputDialog(commands: EditorCommands, state: EditorState, private val parentResource: ParentResource) :
    NewResourceDialog<InputResource>(commands, state, ResourceType.INPUT) {

    override fun createResource() = InputResource(parentResource).apply {
        name = resourceName
        parentResource.children[name] = this
    }

    override fun FormGrid.addRows() {
        newResourceNameRow("", parentResource)
            .textProperty.bidirectionalBind(resourceNameProperty)
    }
}
