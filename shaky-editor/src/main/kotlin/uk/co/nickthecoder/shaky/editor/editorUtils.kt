package uk.co.nickthecoder.shaky.editor

import java.io.File

fun String.containsAnyOf(vararg strings: String): Boolean {
    for (str in strings) {
        if (contains(str)) {
            return true
        }
    }
    return false
}

fun File.openInExternalApp() {
    // TODO This won't work on windows. Will it work on MacOS?
    ProcessBuilder("xdg-open", toString()).start()
}

internal fun openURLInExternalApp(url: String) {
    // TODO This won't work on windows. Will it work on MacOS?
    ProcessBuilder("xdg-open", url).start()
}

fun Int.clamp(min: Int, max: Int) = if (this < min) min else if (this > max) max else this
