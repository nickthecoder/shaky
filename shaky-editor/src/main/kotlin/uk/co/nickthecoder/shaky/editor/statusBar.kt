package uk.co.nickthecoder.shaky.editor

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.scene.dsl.toolBar

fun statusBar(commands: Commands, state: EditorState) = toolBar {
    side = Side.BOTTOM
    + label("Status")
}
