package uk.co.nickthecoder.shaky.editor.tabs

import uk.co.nickthecoder.glok.scene.dsl.checkBox
import uk.co.nickthecoder.glok.scene.dsl.row
import uk.co.nickthecoder.glok.scene.dsl.scrollPane
import uk.co.nickthecoder.glok.scene.dsl.textField
import uk.co.nickthecoder.shaky.core.resources.GameInfo
import uk.co.nickthecoder.shaky.editor.controls.commentsTextArea
import uk.co.nickthecoder.shaky.editor.controls.fullWidthTextField
import uk.co.nickthecoder.shaky.editor.controls.shakyForm
import uk.co.nickthecoder.shaky.editor.controls.xAndY

class GameInfoTab(gameInfo: GameInfo) : StandardResourceTab<GameInfo>(gameInfo) {

    init {
        details = scrollPane {
            content = shakyForm {
                with(gameInfo) {
                    row("Title") {
                        right = fullWidthTextField(title)
                    }
                    row("Full screen?") {
                        right = checkBox("") {
                            selectedProperty.bidirectionalBind(fullScreenProperty)
                        }
                    }
                    row("Maximized?") {
                        right = checkBox("") {
                            selectedProperty.bidirectionalBind(maximizedProperty)
                        }
                    }
                    row("Resizable?") {
                        right = checkBox("") {
                            selectedProperty.bidirectionalBind(resizableProperty)
                        }
                    }
                    row("Default Size") {
                        right = xAndY(gameInfo.widthProperty, gameInfo.heightProperty)
                    }
                    row("Initial Scene Name") {
                        right = textField {
                            textProperty.bidirectionalBind(initialSceneNameProperty)
                        }
                    }
                    row("Test Scene Name") {
                        right = textField {
                            textProperty.bidirectionalBind(testSceneNameProperty)
                        }
                    }
                    row("Producer Name") {
                        right = textField {
                            textProperty.bidirectionalBind(producerNameProperty)
                        }
                    }
                    row("Comments") {
                        right = commentsTextArea {
                            textProperty.bidirectionalBind(commentsProperty)
                        }
                    }
                }
            }
        }
    }
}
