package uk.co.nickthecoder.shaky.editor.tabs

import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.dialog.alertDialog
import uk.co.nickthecoder.glok.event.*
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.UnaryFunction
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.StageType
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.util.Converter
import uk.co.nickthecoder.shaky.core.resources.Input
import uk.co.nickthecoder.shaky.core.resources.InputResource
import uk.co.nickthecoder.shaky.core.resources.KeyInput
import uk.co.nickthecoder.shaky.core.resources.MouseInput
import uk.co.nickthecoder.shaky.editor.Editor
import uk.co.nickthecoder.shaky.editor.controls.horizontalFields
import uk.co.nickthecoder.shaky.editor.controls.resourceNameRow
import uk.co.nickthecoder.shaky.editor.controls.shakyForm

class InputTab(resource: InputResource) : StandardResourceTab<InputResource>(resource) {

    // Kotlin compiler error if <Input> is removed.
    private val listView = listView<Input> {
        cellFactory = { _, item -> createListCell(item) }
        items.addAll(resource.inputs)
    }

    private val inputProperty = listView.selection.selectedItemProperty

    init {
        details = vBox {
            fillWidth = true

            + shakyForm {
                resourceNameRow(resource)
            }

            + Separator()

            + hBox {
                padding(20)
                spacing(10)
                growPriority = 1f
                fillHeight = true

                + listView.apply {
                    overrideMinWidth = 200f
                }

                + singleContainer {
                    growPriority = 1f

                    contentProperty.bindTo(UnaryFunction(inputProperty) { input ->
                        if (input == null) null else inputDetails(input)
                    })
                }
            }

            + hBox {
                padding(6)
                spacing(10)

                + button("Add Key") {
                    graphic = ImageView(Editor.resizableIcons["input_key"])
                    onAction { addKeyInput() }
                }
                + button("Add Mouse") {
                    graphic = ImageView(Editor.resizableIcons["input_mouse"])
                    onAction { addMouseInput() }
                }

                + spacer()

                + button("Delete") {
                    onAction { deleteInput() }
                    disabledProperty.bindTo(inputProperty.isNull())
                }
            }

        }
    }

    private fun addKeyInput() {
        val newInput = KeyInput()
        resource.inputs.add(newInput)
        listView.items.add(newInput)
        inputProperty.value = newInput
    }

    private fun addMouseInput() {
        val newInput = MouseInput()
        resource.inputs.add(newInput)
        listView.items.add(newInput)
        inputProperty.value = newInput
    }

    private fun deleteInput() {
        val input = inputProperty.value ?: return
        listView.items.remove(input)
        resource.inputs.remove(input)
    }

    private fun inputDetails(input: Input) = shakyForm {

        when (input) {
            is KeyInput -> {
                row("Key") {
                    right = horizontalFields {
                        + choiceBox<Key> {
                            items.addAll(Key.values())
                            selection.selectedItemProperty.bidirectionalBind(
                                input.keyProperty,
                                object : Converter<Key?, Key> {
                                    override fun forwards(value: Key?) = value ?: Key.UNKNOWN
                                    override fun backwards(value: Key) = value
                                }
                            )
                        }
                        + button("Pick ...") {
                            onAction {
                                pickKey(scene !!.stage !!, input.keyProperty)
                            }
                        }
                    }
                }
                row("Check Modifiers?") {
                    right = checkBox {
                        selectedProperty.bidirectionalBind(input.checkModifiersProperty)
                    }
                }
                row("Modifiers") {
                    visibleProperty.bindTo(input.checkModifiersProperty)
                    right = horizontalFields {
                        + checkBox("Control") {
                            selectedProperty.bidirectionalBind(input.controlRequiredProperty)
                        }
                        + checkBox("Shift") {
                            selectedProperty.bidirectionalBind(input.shiftRequiredProperty)
                        }
                        + checkBox("Alt") {
                            selectedProperty.bidirectionalBind(input.altRequiredProperty)
                        }
                        + checkBox("Super") {
                            selectedProperty.bidirectionalBind(input.superRequiredProperty)
                        }
                    }
                }
            }

            is MouseInput -> {

                row("Mouse Button") {
                    right = choiceBox<Int> {
                        converter = { index -> if (index == null) "<None>" else MouseInput.buttonLabels[index] }
                        selection.selectedItemProperty.bidirectionalBind(input.buttonIndexProperty,
                            object : Converter<Int?, Int> {
                                override fun backwards(value: Int) = value
                                override fun forwards(value: Int?) = value ?: 0
                            }
                        )
                        items.addAll(0, 1, 2)
                    }
                }
            }
        }
    }

    private fun createListCell(item: Input): ListCell<Input> {

        return TextListCell(listView, item, "").apply {
            padding(8)
            node.textProperty.bindTo(item.displayTextProperty)
            node.graphic = ImageView(Editor.resizableIcons[item.iconName()])
        }

    }
}

private fun pickKey(parentStage: Stage, keyProperty: Property<Key>) {
    alertDialog {
        heading = "Press a key"
        title = "Press a key"

        buttonTypes(ButtonType.CANCEL)
        createStage(parentStage, StageType.MODAL) {
            scene !!.root.addEventFilter(EventType.KEY_PRESSED) { event ->
                keyProperty.value = event.key
                stage.close()
            }
            show()
        }
    }
}
