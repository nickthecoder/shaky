package uk.co.nickthecoder.shaky.editor

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.ApplicationStatus
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.dock.MapDockFactory
import uk.co.nickthecoder.glok.dock.inspector.NodeInspectorDock
import uk.co.nickthecoder.glok.dock.load
import uk.co.nickthecoder.glok.dock.save
import uk.co.nickthecoder.glok.scene.dsl.borderPane
import uk.co.nickthecoder.glok.scene.dsl.documentTabPane
import uk.co.nickthecoder.glok.scene.dsl.scene
import uk.co.nickthecoder.glok.scene.dsl.vBox
import uk.co.nickthecoder.shaky.core.resources.*
import uk.co.nickthecoder.shaky.editor.docks.ResourcesDock
import uk.co.nickthecoder.shaky.editor.tabs.ResourceTab
import java.io.File
import uk.co.nickthecoder.glok.scene.Stage as GlokStage

class MainWindow(glokStage: GlokStage, shakyFile: File) {

    // region ==== Fields ====
    private val tabPane = documentTabPane { }

    private val commands = object : EditorCommands(Editor.icons) {
        override fun openResource(resource: Resource): ResourceTab<*>? = this@MainWindow.openResource(resource)
        override fun deleteResource(resource: Resource) = this@MainWindow.deleteResource(resource)
    }

    private val state = EditorState(glokStage, loadResources(shakyFile), tabPane.selection.selectedItemProperty)

    private val harbour = Harbour()
    private val resourcesDock = ResourcesDock(harbour, commands, state)
    private val nodeInspectorDock = NodeInspectorDock(harbour)
    // endregion fields

    // region ==== Commands ====
    init {
        with(commands) {
            with(EditorActions) {
                FILE_SAVE {
                    saveResources(state.resources, Editor.shakyFile)
                    println("Saved resources")
                }
                FILE_CLOSE_TAB {
                    tabPane.requestFocus()
                    tabPane.tabs.remove(tabPane.selection.selectedItem)
                    tabPane.selection.selectedItem?.content?.requestFocus(true)
                }
                    .disable(state.currentTabProperty.isNull())
                FILE_EXIT { Application.instance.status = ApplicationStatus.REQUEST_QUIT }

                toggle(VIEW_RESOURCES, resourcesDock.visibleProperty)
                toggle(VIEW_NODE_INSPECTOR, nodeInspectorDock.visibleProperty)
            }
        }
    }
    // endregion commands

    // region ==== Init ====
    init {
        GlokSettings.defaultThemeProperty.bindTo(EditorTheme.themeProperty)

        harbour.dockFactory = MapDockFactory(
            resourcesDock,
            nodeInspectorDock
        )
        if (! harbour.load(Editor.preferences())) {
            // Default docks visible if harbour preferences weren't found.
            resourcesDock.visible = true
        }

        glokStage.scene = scene(1000, 800) {
            root = borderPane {
                commands.attachTo(this)

                top = vBox {
                    fillWidth = true
                    + mainMenuBar(commands, state)
                    + mainToolBar(commands, state)
                }
                center = harbour.apply {
                    content = tabPane
                }
                bottom = statusBar(commands, state)
            }
        }
        glokStage.onClosed {
            harbour.save(Editor.preferences())
        }
    }
    // endregion Init

    // region ==== private methods ====
    private fun findTab(resource: Resource) =
        tabPane.tabs.firstOrNull { it is ResourceTab<*> && it.resource === resource } as? ResourceTab<*>

    private fun openResource(resource: Resource): ResourceTab<*>? {
        if (resource is FolderResource) {
            File(Editor.directory(resource.parentResource), resource.name).openInExternalApp()
        }

        var tab: ResourceTab<*>? = findTab(resource)
        if (tab != null) {
            tabPane.selection.selectedItem = tab
        } else {
            tab = ResourceTab.create(resource)
            if (tab != null) {
                tabPane.tabs.add(tab)
            }
        }
        if (tab != null) {
            tab.focusOnContent()
            tabPane.selection.selectedItem = tab
        }
        return tab
    }

    private fun deleteResource(resource: Resource) {
        openResource(resource)?.let {
            // TODO Show "Delete this resource?" confirmation.
        }
    }
    // endregion private methods

}
