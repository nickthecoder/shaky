package uk.co.nickthecoder.shaky.editor.controls

import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.property.boilerplate.FloatProperty
import uk.co.nickthecoder.glok.property.boilerplate.IntProperty
import uk.co.nickthecoder.glok.property.boilerplate.ObservableBoolean
import uk.co.nickthecoder.glok.property.functions.isNotBlank
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.shaky.core.resources.ResourceType
import uk.co.nickthecoder.shaky.editor.Editor


fun fullWidthTextField(text: String = "", block: (TextField.() -> Unit)? = null) = textField {
    this@textField.text = text
    growPriority = 1f
    shrinkPriority = 1f
    block?.invoke(this)
}

fun commentsTextArea(block: TextArea.() -> Unit) = textArea {
    growPriority = 1f
    prefRowCount = 10
    block()
}

fun information(message: String, block: (Label.() -> Unit)? = null) = label(message) {
    textColorProperty.bindTo(Tantalum.fontColor2Property)
    block?.invoke(this)
}

fun errorMessage(block: (Label.() -> Unit)? = null) = label("") {
    styles.add(".errorMessage")
    visibleProperty.bindTo(textProperty.isNotBlank())
    block?.invoke(this)
}

fun horizontalFields(block: HBox.() -> Unit): HBox {
    return hBox {
        alignment = Alignment.CENTER_LEFT
        spacing = 4f
        block()
    }
}

fun xAndY(
    xProperty: IntProperty, yProperty: IntProperty,
    minX: Int = 0, minY: Int = 0,
    maxX: Int = Int.MAX_VALUE, maxY: Int = Int.MAX_VALUE,
    readOnlyProperty: ObservableBoolean? = null
) = horizontalFields {
    + intSpinner {
        min = minX
        max = maxX
        shrinkPriority = 0.5f
        editor.prefColumnCount = 5
        valueProperty.bidirectionalBind(xProperty)
        readOnlyProperty?.let { this.readOnlyProperty.bindTo(it) }
    }
    + label("x") {
        //shrinkPriority = 100f
    }
    + intSpinner {
        min = minY
        max = maxY
        shrinkPriority = 0.5f
        editor.prefColumnCount = 5
        valueProperty.bidirectionalBind(yProperty)
        readOnlyProperty?.let { this.readOnlyProperty.bindTo(it) }
    }
}

fun xAndY(
    xProperty: FloatProperty, yProperty: FloatProperty,
    minX: Float = - Float.MAX_VALUE, minY: Float = - Float.MAX_VALUE,
    maxX: Float = - Float.MAX_VALUE, maxY: Float = - Float.MAX_VALUE,
    readOnlyProperty: ObservableBoolean? = null

) = horizontalFields {
    + floatSpinner {
        min = minX
        max = maxX
        shrinkPriority = 0.5f
        editor.prefColumnCount = 5
        valueProperty.bidirectionalBind(xProperty)
        readOnlyProperty?.let { this.readOnlyProperty.bindTo(it) }
    }
    + label("x") {
            //shrinkPriority = 100f
    }
    + floatSpinner {
        min = minY
        max = maxY
        shrinkPriority = 0.5f
        editor.prefColumnCount = 5
        valueProperty.bidirectionalBind(yProperty)
        readOnlyProperty?.let { this.readOnlyProperty.bindTo(it) }
    }
}

fun FormGrid.isValid(): Boolean {
    fun invalidRow(): FormRow? {
        for (row in children) {
            val below = row.below ?: continue

            if (below.visible) {
                if (below.styles.contains(".errorMessage")) {
                    return row
                }
            }
        }
        return null
    }

    val errorRow = invalidRow()
    if (errorRow != null) {
        (parent as? ScrollPane)?.scrollTo(errorRow)
        errorRow.right?.requestFocus(true)
    }
    return errorRow == null
}

fun shakyForm(block: FormGrid.() -> Unit) = formGrid {
    style(".form")
    block()
}

fun heading(heading: String, block: (Label.() -> Unit)? = null) = label(heading) {
    style(".heading")
    block?.invoke(this)
}

fun ResourceType.graphic() = ImageView(Editor.resizableIcons[iconName()])
