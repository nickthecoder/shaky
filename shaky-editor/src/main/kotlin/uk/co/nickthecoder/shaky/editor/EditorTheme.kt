package uk.co.nickthecoder.shaky.editor

import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.property.boilerplate.ColorUnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.themeProperty
import uk.co.nickthecoder.glok.property.functions.withAlpha
import uk.co.nickthecoder.glok.property.invalidationListener
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.text.Font
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.Theme
import uk.co.nickthecoder.glok.theme.dsl.theme

object EditorTheme {

    val errorColorProperty = ColorUnaryFunction(Tantalum.darkProperty) { dark ->
        if (dark) Color["#833"] else Color["#833"]
    }
    val errorColor by errorColorProperty

    val highlightColorProperty = Tantalum.accentColorProperty.withAlpha(0.2f)
    val highlightColor by highlightColorProperty

    val gridColorProperty = Tantalum.accentColorProperty
    val gridColor by gridColorProperty

    val crossHairsColorProperty = errorColorProperty
    val crossHairsColor by crossHairsColorProperty

    val themeProperty by themeProperty(buildTheme())
    var theme by themeProperty

    init {
        val listener = invalidationListener {
            // A bodge to ensure that changes to each property doesn't rebuild the theme immediately,
            // and then the change to another property ALSO rebuilds the theme.
            // Without this, the theme is rebuilt many times when flipping from dark to light theme.
            var needsRebuilding = true
            Platform.runLater {
                if (needsRebuilding) {
                    theme = buildTheme()
                }
                needsRebuilding = false
            }
        }
        for (prop in arrayOf(Tantalum.themeProperty)) {
            prop.addListener(listener)
        }
    }

    private fun buildTheme(): Theme {
        val localTheme = theme {
            ".form" {
                padding(10)
                "columnSpacing"(16f)
            }
            ".errorMessage" {
                textColor(errorColor)
            }
            ".heading" {
                padding(12)
                plainBackground(Tantalum.background2Color)
                borderSize(0, 0, 1, 0)
                plainBorder(Tantalum.strokeColor)
                font(Font.basedOn(Font.defaultFont, size = 20f))
            }

            "grid" {
                "gridColor"(gridColor)
            }
            "highlight_pose" {
                "crossHairsColor"(crossHairsColor)
                "highlightColor"(highlightColor)
            }
            "select_scattered_pose" {
                "highlightColor"(highlightColor)
            }
        }
        return Tantalum.theme.combineWith(localTheme)
    }
}
