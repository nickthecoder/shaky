package uk.co.nickthecoder.shaky.editor

import uk.co.nickthecoder.glok.control.Tab
import uk.co.nickthecoder.glok.property.ObservableValue
import uk.co.nickthecoder.shaky.core.resources.Resources
import uk.co.nickthecoder.glok.scene.Stage as GlokStage

class EditorState(
    val glokStage: GlokStage,
    val resources: Resources,
    val currentTabProperty: ObservableValue<Tab?>
)
