package uk.co.nickthecoder.shaky.editor

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.scene.NamedImages
import uk.co.nickthecoder.shaky.core.resources.Resource
import uk.co.nickthecoder.shaky.editor.tabs.ResourceTab

abstract class EditorCommands(icons: NamedImages) : Commands(icons) {

    abstract fun openResource(resource: Resource): ResourceTab<*>?
    abstract fun deleteResource(resource: Resource)

}
