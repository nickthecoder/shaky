// Shaky top-level build script

plugins {
    kotlin("jvm")
    //id("maven-publish")
}

val shakyVersion: String by project
val glokVersion: String by project

dependencies {
    implementation("uk.co.nickthecoder:glok-model:$glokVersion")
    implementation("uk.co.nickthecoder:glok-core:$glokVersion")
    implementation("uk.co.nickthecoder:glok-dock:$glokVersion")
    implementation( project(":shaky-core") )
}
