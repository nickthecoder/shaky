// shaky-core build script

plugins {
    kotlin("jvm")
    //id("maven-publish")
}

val shakyVersion: String by project
val glokVersion: String by project
val jsonVersion: String by project

dependencies {
    implementation("uk.co.nickthecoder:glok-model:$glokVersion")
    implementation("uk.co.nickthecoder:glok-core:$glokVersion")
    implementation( "com.eclipsesource.minimal-json:minimal-json:${jsonVersion}" )

}

/*
Haven't set up the project on GitLab yet!
publishing {
    repositories {

        maven {
            // 46354938 is the GitLab project ID of project nickthecoder/glok
            url = uri("https://gitlab.com/api/v4/projects/46354938/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = providers.gradleProperty("gitLabPrivateToken").get()
                // The password is stored in ~/.gradle/gradle.properties
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}
*/
