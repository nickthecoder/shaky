package uk.co.nickthecoder.shaky.core.resources

import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.event.KeyEvent
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.property

interface Input {

    val displayTextProperty: ObservableString
    val displayText: String

    fun isPressed(): Boolean
    fun matches(event: KeyEvent): Boolean
    fun matches(event: MouseEvent): Boolean

    fun iconName() : String

    companion object {
        val keyboardState = BooleanArray(Key.values().size)
        val mouseState = BooleanArray(3)
        fun controlPressed() = keyboardState[Key.LEFT_CONTROL.ordinal] || keyboardState[Key.RIGHT_CONTROL.ordinal]
        fun shiftPressed() = keyboardState[Key.LEFT_SHIFT.ordinal] || keyboardState[Key.RIGHT_SHIFT.ordinal]
        fun altPressed() = keyboardState[Key.LEFT_ALT.ordinal] || keyboardState[Key.RIGHT_ALT.ordinal]
        fun superPressed() = keyboardState[Key.LEFT_SUPER.ordinal] || keyboardState[Key.RIGHT_SUPER.ordinal]
    }
}

abstract class ModifiersInput : Input {

    val checkModifiersProperty by booleanProperty(false)
    var checkModifiers by checkModifiersProperty

    val shiftRequiredProperty by booleanProperty(false)
    var shiftRequired by shiftRequiredProperty

    val controlRequiredProperty by booleanProperty(false)
    var controlRequired by controlRequiredProperty

    val altRequiredProperty by booleanProperty(false)
    var altRequired by altRequiredProperty

    val superRequiredProperty by booleanProperty(false)
    var superRequired by superRequiredProperty

    val modifiersTextProperty: ObservableString = StringUnaryFunction(checkModifiersProperty) { check ->
        if (check) {
            StringBuilder().apply {
                if (controlRequired) append("Ctrl+")
                if (altRequired) append("Alt+")
                if (shiftRequired) append("Shift+")
                if (superRequired) append("Super+")
            }.toString()
        } else {
            ""
        }
    }.apply {
        addDependent(controlRequiredProperty)
        addDependent(shiftRequiredProperty)
        addDependent(altRequiredProperty)
        addDependent(superRequiredProperty)
    }

    override fun isPressed() = if (checkModifiers) {
        Input.controlPressed() == controlRequired &&
            Input.shiftPressed() == shiftRequired &&
            Input.altPressed() == altRequired &&
            Input.superPressed() == superRequired
    } else {
        true
    }

    override fun matches(event: KeyEvent) = if (checkModifiers) {
        event.isControlDown == controlRequired &&
            event.isShiftDown == shiftRequired &&
            event.isAltDown == altRequired &&
            event.isSuperDown == superRequired
    } else {
        true
    }

    override fun matches(event: MouseEvent) = if (checkModifiers) {
        event.isControlDown == controlRequired &&
            event.isShiftDown == shiftRequired &&
            event.isAltDown == altRequired &&
            event.isSuperDown == superRequired
    } else {
        true
    }

}

class KeyInput : ModifiersInput() {

    val keyProperty by property(Key.UNKNOWN)
    var key by keyProperty

    override val displayTextProperty: ObservableString =
        StringBinaryFunction(keyProperty, modifiersTextProperty) { key, modifiers ->
            key.label + modifiers
        }
    override val displayText by displayTextProperty

    override fun iconName() = "input_key"

    override fun isPressed() = Input.keyboardState[key.ordinal] && super.isPressed()

    override fun matches(event: KeyEvent) = event.key === key && super.matches(event)

    override fun matches(event: MouseEvent) = false

}

class MouseInput : ModifiersInput() {


    val buttonIndexProperty by intProperty(0)
    var buttonIndex by buttonIndexProperty

    override val displayTextProperty: ObservableString =
        StringBinaryFunction(buttonIndexProperty, modifiersTextProperty) { buttonIndex, modifiers ->
            buttonLabels[buttonIndex] + modifiers
        }
    override val displayText by displayTextProperty


    override fun iconName() = "input_mouse"

    override fun isPressed() = Input.mouseState[buttonIndex] && super.isPressed()

    override fun matches(event: KeyEvent) = false

    override fun matches(event: MouseEvent) = Input.mouseState[event.buttonIndex] && super.matches(event)

    companion object {
        val buttonLabels = arrayOf("Left", "Right", "Middle")
    }
}
