package uk.co.nickthecoder.shaky.core.resources

import uk.co.nickthecoder.glok.collections.asMutableObservableMap

class ScatteredTextureResource(parent: ParentResource) : TextureResource(parent), ParentResource {

    override val textureType: TextureType
        get() = TextureType.SCATTERED

    override val children = mutableMapOf<String, Resource>().asMutableObservableMap()
}
