package uk.co.nickthecoder.shaky.core.resources

import uk.co.nickthecoder.glok.property.boilerplate.*

class GridPose(
    override val textureResource: GridTextureResource,
    val x: Int,
    val y: Int
) : AbstractResource(textureResource), Pose {

    override val leftProperty = IntTernaryFunction(
        textureResource.xPaddingProperty,
        textureResource.xSpacingProperty,
        textureResource.poseWidthProperty
    ) { padding, spacing, width -> padding + x * (width + spacing) }
    override val left: Int by leftProperty

    override val topProperty = IntTernaryFunction(
        textureResource.yPaddingProperty,
        textureResource.ySpacingProperty,
        textureResource.poseHeightProperty
    ) { padding, spacing, height -> padding + y * (height + spacing) }
    override val top by topProperty

    override val widthProperty: ObservableInt
        get() = textureResource.poseWidthProperty
    override val width: Int by widthProperty

    override val heightProperty: ObservableInt
        get() = textureResource.poseHeightProperty
    override val height: Int by heightProperty


    override val xOffsetProperty by intProperty(0)
    override var xOffset by xOffsetProperty

    override val yOffsetProperty by intProperty(0)
    override var yOffset by yOffsetProperty

    override val imageProperty: ObservableImage =
        ImageTernaryFunction(textureResource.textureProperty, leftProperty, topProperty) { texture, left, top ->
            texture.partialImage(
                left.toFloat(), top.toFloat(),
                width.toFloat(), height.toFloat()
            )
        }.apply {
            addDependent(widthProperty)
            addDependent(heightProperty)
        }
    override val image by imageProperty

}
