package uk.co.nickthecoder.shaky.core.resources

import uk.co.nickthecoder.glok.collections.asMutableObservableMap

class FolderResource(parentResource: ParentResource) : AbstractResource(parentResource), ParentResource {

    override val resourceType get() = ResourceType.FOLDER

    override val children = mutableMapOf<String, Resource>().asMutableObservableMap()

    init {
        // Update the resources map when name is changed.
        nameProperty.addChangeListener { _, oldName, newName ->
            parentResource.children.remove(oldName)
            parentResource.children[newName] = this
        }
    }
}
