package uk.co.nickthecoder.shaky.core.resources

enum class TextureType(val title: String) {
    SINGLE("Single Image"),
    GRID("Grid"),
    SCATTERED("Scattered")
}
