package uk.co.nickthecoder.shaky.core.resources

import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Image

interface Pose : Resource {

    override val resourceType: ResourceType
        get() = ResourceType.POSE

    val textureResource: TextureResource

    val topProperty: ObservableInt
    val top: Int

    val leftProperty: ObservableInt
    val left: Int

    val widthProperty: ObservableInt
    val width: Int

    val heightProperty: ObservableInt
    val height: Int

    val xOffsetProperty: ObservableInt
    val xOffset: Int

    val yOffsetProperty: ObservableInt
    val yOffset: Int

    val imageProperty: ObservableImage
    val image: Image
}

