package uk.co.nickthecoder.shaky.core.resources

import uk.co.nickthecoder.glok.collections.asMutableObservableMap
import uk.co.nickthecoder.glok.property.boilerplate.SimpleStringProperty
import uk.co.nickthecoder.glok.backend.Resources as GlokResources

class Resources(val glokResources: GlokResources) : ParentResource {

    override val parentResource: ParentResource?
        get() = null

    override val pathProperty = SimpleStringProperty("/")
    override val path by pathProperty

    override val root get() = this
    val gameInfo = GameInfo(this)

    override val children = mutableMapOf<String, Resource>().asMutableObservableMap()

    var isMock = false

}
