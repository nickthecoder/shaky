package uk.co.nickthecoder.shaky.core.resources

import uk.co.nickthecoder.glok.collections.asMutableObservableMap
import uk.co.nickthecoder.glok.property.boilerplate.IntTernaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.ObservableInt
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.intProperty

class GridTextureResource(parent: ParentResource) : TextureResource(parent), ParentResource {

    override val textureType: TextureType
        get() = TextureType.GRID

    val xSpacingProperty by intProperty(1)
    var xSpacing by xSpacingProperty

    val ySpacingProperty by intProperty(1)
    var ySpacing by ySpacingProperty

    val xPaddingProperty by intProperty(1)
    var xPadding by xPaddingProperty

    val yPaddingProperty by intProperty(1)
    var yPadding by yPaddingProperty

    val poseWidthProperty by intProperty(1)
    var poseWidth by poseWidthProperty

    val poseHeightProperty by intProperty(1)
    var poseHeight by poseHeightProperty

    val sharedOffsetsProperty by booleanProperty(false)
    var sharedOffsets by sharedOffsetsProperty

    val sharedXOffsetProperty by intProperty(0)
    var sharedXOffset by sharedXOffsetProperty

    val sharedYOffsetProperty by intProperty(0)
    var sharedYOffset by sharedYOffsetProperty

    val acrossProperty: ObservableInt =
        IntTernaryFunction(poseWidthProperty, xPaddingProperty, ySpacingProperty) { poseWidth, xPadding, xSpacing ->
            (texture.width - xPadding + xSpacing) / (poseWidth + xSpacing)
        }.apply { addDependent(textureProperty) }
    val across by acrossProperty

    val downProperty: ObservableInt =
        IntTernaryFunction(poseHeightProperty, yPaddingProperty, ySpacingProperty) { poseHeight, yPadding, ySpacing ->
            (texture.height - yPadding + ySpacing) / (poseHeight + ySpacing)
        }.apply { addDependent(textureProperty) }
    val down by downProperty

    private val grid = mutableListOf<MutableList<GridPose?>>()

    override val children = mutableMapOf<String, Resource>().asMutableObservableMap()

    init {
        sharedOffsetsProperty.addChangeListener { _, _, shared ->
            for (column in grid) {
                for (pose in column) {
                    if (pose != null) {
                        if (shared) {
                            pose.xOffsetProperty.bindTo(sharedXOffsetProperty)
                            pose.yOffsetProperty.bindTo(sharedYOffsetProperty)
                        } else {
                            pose.xOffsetProperty.unbind()
                            pose.yOffsetProperty.unbind()
                        }
                    }
                }
            }
        }
        children.addChangeListener { _, changes ->
            if (sharedOffsets) {
                for (change in changes) {
                    if (change.isRemoval) {
                        (change.oldValue as? GridPose)?.let { pose ->
                            pose.xOffsetProperty.unbind()
                            pose.yOffsetProperty.unbind()
                        }
                    } else {
                        (change.value as? GridPose)?.let { pose ->
                            pose.xOffsetProperty.bindTo(sharedXOffsetProperty)
                            pose.yOffsetProperty.bindTo(sharedYOffsetProperty)
                        }
                    }
                }
            }
        }
    }


    fun findPose(x: Int, y: Int): GridPose? {
        if (x < 0 || x >= across) throw IndexOutOfBoundsException("X Index $x not in range 0..$across")
        if (y < 0 || y >= down) throw IndexOutOfBoundsException("Y Index $y not in range 0..$down")

        if (x > grid.size - 1) return null
        val column = grid[x]
        if (y > column.size - 1) return null
        return column[y]
    }

    // TODO getPose(x,y) which returns a Dummy if out of range, or missing.

    fun addPose(pose: GridPose) {
        val x = pose.x
        val y = pose.y

        if (x < 0 || x >= across) throw IndexOutOfBoundsException("X Index $x not in range 0..$across")
        if (y < 0 || y >= down) throw IndexOutOfBoundsException("Y Index $y not in range 0..$down")
        while (x >= grid.size) {
            grid.add(mutableListOf())
        }
        val column = grid[x]
        while (y >= column.size) {
            column.add(null)
        }
        column[y] = pose
    }

    fun remove(x: Int, y: Int) {
        val pose = findPose(x, y)
        if (pose != null) {
            grid[x][y] = null
            children.remove(pose.name)
        }
    }
}
