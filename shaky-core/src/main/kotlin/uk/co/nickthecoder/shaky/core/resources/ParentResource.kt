package uk.co.nickthecoder.shaky.core.resources

import uk.co.nickthecoder.glok.collections.MutableObservableMap
import uk.co.nickthecoder.glok.property.boilerplate.ObservableString
import uk.co.nickthecoder.glok.util.log

interface ParentResource {

    val parentResource: ParentResource?

    val pathProperty: ObservableString
    val path: String

    val root: Resources
        get() = parentResource !!.root

    val children: MutableObservableMap<String, Resource>

    fun find(pathParts: List<String>): Resource? {
        if (pathParts.isEmpty()) return null
        if (pathParts.size == 1) find(pathParts.first())

        val first = pathParts.first()
        val remainder = pathParts.subList(1, pathParts.size)
        return if (first == "..") {
            parentResource?.find(remainder)
        } else {
            folder(first)?.find(remainder)
        }
    }

    fun find(path: String): Resource? = if (path.startsWith('/')) {
        root.texture(path.substring(1))
    } else {
        if (path.contains('/')) {
            find(path.split('/'))
        } else {
            children[path] as? TextureResource
        }
    }

    fun texture(path: String) = find(path) as? TextureResource
    fun input(path: String) = find(path) as? InputResource
    fun folder(path: String) = children[path] as? FolderResource
    fun pose(path: String) = children[path] as? Pose

    /**
     * A non-nullable version of [input].
     * If the input is not found, then a dummy InputResource is returned, which matches no events.
     */
    fun safeInput(path: String) = input(path) ?: InputResource(this).also {
        if (root.isMock) {
            log.warn("Input ${this.path} / $path not found")
        }
    }

    //fun getFolder(path: String) = findFolder(path) ?: FolderResource(this)

    fun unusedName(prefix: String): String {
        if (find(prefix) == null) return prefix
        var suffix = 1
        do {
            val newName = "$prefix$suffix"
            if (find(prefix) == null) return newName

            suffix ++
        } while (true)
    }
}
