package uk.co.nickthecoder.shaky.core.resources

import uk.co.nickthecoder.glok.collections.asMutableObservableList

class InputResource(parent: ParentResource) : AbstractResource(parent) {

    override val resourceType: ResourceType
        get() = ResourceType.INPUT

    val inputs = mutableListOf<Input>().asMutableObservableList()

    init {
        // TODO Is this to be duplicated for each resource type?
        // Update the resources map when name is changed.
        nameProperty.addChangeListener { _, oldName, newName ->
            parent.children.remove(oldName)
            parent.children[newName] = this
        }
    }
}
