package uk.co.nickthecoder.shaky.core.resources

import uk.co.nickthecoder.glok.property.boilerplate.*

class SingleTextureResource(parent: ParentResource) : TextureResource(parent), Pose {

    // TODO Hmm, this is a Texture AND a Pose ???
    override val textureType: TextureType
        get() = TextureType.SINGLE

    override val resourceType: ResourceType
        get() = ResourceType.TEXTURE

    override val textureResource: TextureResource
        get() = this

    override val leftProperty: ObservableInt
        get() = SimpleIntProperty(0).asReadOnly()
    override val left by leftProperty

    override val topProperty: ObservableInt
        get() = SimpleIntProperty(0).asReadOnly()
    override val top by topProperty

    override val widthProperty: ObservableInt = IntUnaryFunction(textureResource.textureProperty) {
        it.width
    }
    override val width by widthProperty

    override val heightProperty: ObservableInt = IntUnaryFunction(textureResource.textureProperty) {
        it.width
    }
    override val height by heightProperty

    override val xOffsetProperty by intProperty(0)
    override var xOffset by xOffsetProperty

    override val yOffsetProperty by intProperty(0)
    override var yOffset by yOffsetProperty

    override val imageProperty: ObservableImage =
        ImageUnaryFunction(textureResource.textureProperty) { texture ->
            texture
        }
    override val image by imageProperty
}
