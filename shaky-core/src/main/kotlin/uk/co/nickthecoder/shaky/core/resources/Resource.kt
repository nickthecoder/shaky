package uk.co.nickthecoder.shaky.core.resources

import uk.co.nickthecoder.glok.property.boilerplate.*

interface Resource : Comparable<Resource> {

    val parentResource: ParentResource

    val resourceType: ResourceType

    val nameProperty: StringProperty
    var name: String

    val pathProperty: ObservableString
    val path: String

    val commentProperty: StringProperty
    var comment: String

    override fun compareTo(other: Resource): Int {
        val rt = resourceType.compareTo(other.resourceType)
        return if (rt == 0) {
            name.compareTo(other.name)
        } else {
            rt
        }
    }

}

abstract class AbstractResource(

    final override val parentResource: ParentResource,
    name: String = ""

) : Resource {

    final override val nameProperty by validatedStringProperty(name) { _, old, new ->
        val existing = parentResource.children[new]
        if (new.isNotBlank() && (existing == null || existing == this)) new else old
    }
    final override var name by nameProperty

    final override val pathProperty = StringBinaryFunction(parentResource.pathProperty, nameProperty) { parent, name ->
        "$parent/$name"
    }
    final override val path by pathProperty

    final override val commentProperty by stringProperty("")
    final override var comment by commentProperty

    init {
        nameProperty.addChangeListener { _, old, new ->
            val children = parentResource.children
            children.remove(old)
            children[new] = this
        }
    }
}
