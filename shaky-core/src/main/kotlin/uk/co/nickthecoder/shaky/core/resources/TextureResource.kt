package uk.co.nickthecoder.shaky.core.resources

import uk.co.nickthecoder.glok.backend.Texture
import uk.co.nickthecoder.glok.collections.asMutableObservableMap
import uk.co.nickthecoder.glok.property.ObservableValue
import uk.co.nickthecoder.glok.property.UnaryFunction

abstract class TextureResource(parent: ParentResource) : AbstractResource(parent), ParentResource {

    abstract val textureType: TextureType

    override val resourceType: ResourceType
        get() = ResourceType.TEXTURE

    val textureProperty: ObservableValue<Texture> =
        UnaryFunction(pathProperty) { path -> parent.root.glokResources.loadTexture(path) }
    val texture by textureProperty

    init {
        // TODO Is this to be duplicated for each resource type?
        // Update the resources map when name is changed.
        nameProperty.addChangeListener { _, oldName, newName ->
            parent.children.remove(oldName)
            parent.children[newName] = this
        }
    }

    override val children = mutableMapOf<String, Resource>().asMutableObservableMap()

    companion object {
        fun create(textureType: TextureType, parent: ParentResource): TextureResource =
            when (textureType) {
                TextureType.SINGLE -> SingleTextureResource(parent)
                TextureType.SCATTERED -> ScatteredTextureResource(parent)
                TextureType.GRID -> GridTextureResource(parent)
            }
    }
}

