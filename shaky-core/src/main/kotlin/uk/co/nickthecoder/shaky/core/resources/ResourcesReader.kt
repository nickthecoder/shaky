package uk.co.nickthecoder.shaky.core.resources

import com.eclipsesource.json.Json
import com.eclipsesource.json.JsonObject
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.event.Key
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader

fun loadResources(shakyFile: File) = ResourcesReader(shakyFile).load()
fun loadGameInfo(shakyFile: File) = ResourcesReader(shakyFile).loadInfo()

private class ResourcesReader(val shakyFile: File) {

    private lateinit var resources: Resources
    private val jsonTagStack = mutableListOf<String>()

    fun load(): Resources {
        resources = Resources(backend.fileResources(shakyFile.parent))
        with(Json.parse(InputStreamReader(FileInputStream(shakyFile))).asObject()) {
            getObject("info") { loadInfo() }
            loadParent(resources)
        }
        return resources
    }

    fun loadInfo(): GameInfo {
        resources = Resources(backend.fileResources(shakyFile.parent))

        with(Json.parse(InputStreamReader(FileInputStream(shakyFile))).asObject()) {
            getObject("info") { loadInfo() }
        }
        return resources.gameInfo
    }

    fun JsonObject.loadParent(parent: ParentResource) {
        loadInputs(parent)
        loadTextures(parent)
        loadFolders(parent)
    }

    // region ==== Game Info ====
    private fun JsonObject.loadInfo() {
        with(resources.gameInfo) {
            title = getString("name", title)
            width = getInt("width", width)
            height = getInt("height", height)
            resizable = getBoolean("resizable", resizable)
            fullScreen = getBoolean("fullScreen", fullScreen)
            initialSceneName = getString("initialScene", initialSceneName)
            testSceneName = getString("testScene", testSceneName)
            //enableFeather = getBoolean( "enableFeather", enableFeather )
            producerName = getString("producer", producerName)
            comments = getString("comments", comments)

            // If there are known incompatibilities, we should check the version, and
            // issue a warning/error.
            shakyVersion = getString("shakyVersion", shakyVersion)
        }
    }
    // endregion GameInfo

    // region ==== Folders ====
    fun JsonObject.loadFolders(parent: ParentResource) {
        getArray("folders") {
            val folder = FolderResource(parent)
            with(folder) {
                name = getString("name", name)
                parent.children[name] = this
            }
            loadParent(folder)
        }
    }
    // endregion Folders

    // region ==== Inputs ====
    fun JsonObject.loadInputs(parent: ParentResource) {
        getArray("inputs") {
            val inputResource = InputResource(parent)
            with(inputResource) {
                name = getString("name", name)
                parent.children[name] = this

                getArray("items") {
                    val type = getString("type", "")
                    when (type) {
                        "KeyInput" -> {
                            KeyInput().apply {

                                key = getEnum("key", Key.UNKNOWN)

                                checkModifiers = getBoolean("checkModifiers", checkModifiers)
                                if (checkModifiers) {
                                    controlRequired = getBoolean("control", false)
                                    shiftRequired = getBoolean("shiftRequired", false)
                                    altRequired = getBoolean("altRequired", false)
                                    superRequired = getBoolean("superRequired", false)
                                }

                                inputs.add(this)
                            }
                        }

                        "MouseInput" -> {
                            MouseInput().apply {
                                buttonIndex = getInt("button", 0)

                                checkModifiers = getBoolean("checkModifiers", checkModifiers)
                                if (checkModifiers) {
                                    controlRequired = getBoolean("control", false)
                                    shiftRequired = getBoolean("shiftRequired", false)
                                    altRequired = getBoolean("altRequired", false)
                                    superRequired = getBoolean("superRequired", false)
                                }

                                inputs.add(this)
                            }
                        }

                        else -> println("Unknown input type : $type")
                    }
                }
            }
        }
    }
    // endregion Inputs

    // region ==== Textures ====
    fun JsonObject.loadTextures(parent: ParentResource) {
        getArray("textures") {
            val textureType = getEnum("type", TextureType.SINGLE)
            val textureResource = TextureResource.create(textureType, parent)
            textureResource.name = getString("name", "")

            when (textureResource) {
                is GridTextureResource -> {
                    with(textureResource) {
                        xPadding = getInt("xPadding", xPadding)
                        yPadding = getInt("yPadding", yPadding)
                        xSpacing = getInt("xSpacing", xSpacing)
                        ySpacing = getInt("ySpacing", ySpacing)
                        poseWidth = getInt("poseWidth", poseWidth)
                        poseHeight = getInt("poseHeight", poseHeight)

                        sharedOffsets = getBoolean("sharedOffsets", false)
                        if (sharedOffsets) {
                            sharedXOffset = getInt("sharedXOffset", 0)
                            sharedYOffset = getInt("sharedYOffset", 0)
                        }
                    }
                }
            }

            parent.children[textureResource.name] = textureResource

            when (textureResource) {
                is GridTextureResource -> loadGridPoses(textureResource)
                is ScatteredTextureResource -> loadScatteredPoses(textureResource)
                is SingleTextureResource -> {
                    textureResource.xOffset = getInt("xOffset", textureResource.xOffset)
                    textureResource.yOffset = getInt("yOffset", textureResource.yOffset)
                }
            }
        }
    }

    fun JsonObject.loadGridPoses(grid: GridTextureResource) {
        getArray("gridPoses") {
            val x = getInt("x", - 1)
            val y = getInt("y", - 1)
            if (x >= 0 && x < grid.across && y >= 0 && y < grid.down) {
                with(GridPose(grid, x, y)) {
                    name = getString("name", "")
                    if (! grid.sharedOffsets) {
                        xOffset = getInt("xOffset", xOffset)
                        yOffset = getInt("yOffset", yOffset)
                    }
                    grid.addPose(this)
                }
            }
        }
    }

    fun JsonObject.loadScatteredPoses(scatteredTextureResource: ScatteredTextureResource) {
        getArray("poses") {
            with(ScatteredPose(scatteredTextureResource)) {
                name = getString("name", scatteredTextureResource.unusedName("<unnamed>"))
                top = getInt("top", top)
                left = getInt( "left", left)
                width = getInt("width", width)
                height = getInt( "height", height)
                xOffset = getInt("xOffset", xOffset)
                yOffset = getInt("yOffset", yOffset)
                scatteredTextureResource.children[name] = this
            }
        }
    }

    // endregion Textures

    // region ==== Helper Functions ====
    private fun JsonObject.getObject(name: String, block: JsonObject.() -> Unit) {
        val value = get(name)
        if (value.isObject) {
            jsonTagStack.add(name)
            value.asObject().block()
            jsonTagStack.removeLast()
        } else {
            throw ResourcesException("Expected an JSON object named $name at $jsonTagStack")
        }
    }


    private fun JsonObject.getArray(name: String, block: JsonObject.() -> Unit) {
        val value = get(name) ?: return
        if (value.isArray) {
            val array = value.asArray()
            jsonTagStack.add(name)
            for ((index, element) in array.withIndex()) {
                if (element.isObject) {
                    jsonTagStack.add("#$index")
                    element.asObject().block()
                    jsonTagStack.removeLast()
                } else {
                    throw ResourcesException("Expected a JSON array named $name at $jsonTagStack")
                }
            }
            jsonTagStack.removeLast()
        } else {
            throw ResourcesException("Expected a JSON array named $name at $jsonTagStack")
        }
    }

    private inline fun <reified T : Enum<T>> valueOf(name: String, default: T): T {
        return try {
            java.lang.Enum.valueOf(T::class.java, name)
        } catch (e: Exception) {
            println("WARNING : Enum type '$name' not found. Defaulting to : $default")
            default
        }
    }

    private inline fun <reified T : Enum<T>> JsonObject.getEnum(name: String, default: T): T {
        val enumName = getString(name, default.name)
        return valueOf(enumName, default)
    }
    // endregion Helper Functions
}
