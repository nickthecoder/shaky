package uk.co.nickthecoder.shaky.core.resources

class ResourcesException(message: String) : Exception(message)
