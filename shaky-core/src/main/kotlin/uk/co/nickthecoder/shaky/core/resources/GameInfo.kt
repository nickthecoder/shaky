package uk.co.nickthecoder.shaky.core.resources

import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty
import uk.co.nickthecoder.shaky.core.SHAKY_VERSION

class GameInfo(parentResources: ParentResource) : AbstractResource(parentResources, "Game Info") {

    override val resourceType: ResourceType
        get() = ResourceType.GAME_INFO

    val titleProperty by stringProperty("New Game")
    var title by titleProperty

    val fullScreenProperty by booleanProperty(false)
    var fullScreen by fullScreenProperty

    val widthProperty by intProperty(800)
    var width by widthProperty

    val heightProperty by intProperty(600)
    var height by heightProperty

    val maximizedProperty by booleanProperty(false)
    var maximized by maximizedProperty

    val resizableProperty by booleanProperty(true)
    var resizable by resizableProperty

    val initialSceneNameProperty by stringProperty("")
    var initialSceneName by initialSceneNameProperty

    val testSceneNameProperty by stringProperty("")
    var testSceneName by testSceneNameProperty

    val producerNameProperty by stringProperty("")
    var producerName by producerNameProperty

    val commentsProperty by stringProperty("")
    var comments by commentsProperty

    val shakyVersionProperty by stringProperty(SHAKY_VERSION)
    var shakyVersion by shakyVersionProperty

}
