package uk.co.nickthecoder.shaky.core.resources

enum class ResourceType(val title: String) : Comparable<ResourceType> {
    GAME("Game"),
    GAME_INFO("Game Info"),
    TEXTURE("Texture"),
    POSE("Pose"),
    INPUT("Input"),
    FOLDER("Folder");

    fun iconName() = "resource_${name.lowercase()}"
}
