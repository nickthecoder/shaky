package uk.co.nickthecoder.shaky.core.resources

import com.eclipsesource.json.JsonArray
import com.eclipsesource.json.JsonObject
import com.eclipsesource.json.WriterConfig
import uk.co.nickthecoder.shaky.core.SHAKY_VERSION
import java.io.BufferedWriter
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStreamWriter

fun saveResources(resources: Resources, shakyFile: File) {
    ResourcesWriter(resources, shakyFile).save()
}

private class ResourcesWriter(val resources: Resources, val shakyFile: File) {

    fun save() {

        JsonObject().apply {
            add("info", saveInfo())
            saveParent(resources)

            BufferedWriter(OutputStreamWriter(FileOutputStream(shakyFile))).use {
                writeTo(it, WriterConfig.PRETTY_PRINT)
            }
        }
    }

    fun JsonObject.saveParent(parent: ParentResource) {
        addArray("inputs", saveInputs(parent))
        addArray("textures", saveTextures(parent))
        addArray("folders", saveFolders(parent))
    }

    // region ==== GameInfo ====

    private fun saveInfo(): JsonObject {
        val default = GameInfo(resources)
        return JsonObject().apply {
            with(resources.gameInfo) {
                add("name", title)
                add("width", width)
                add("height", height)
                addDefault("resizable", resizable, default.resizable)
                addDefault("fullScreen", fullScreen, default.fullScreen)
                addDefault("initialScene", initialSceneName, default.initialSceneName)
                addDefault("testScene", testSceneName, default.testSceneName)
                //addDefault("enableFeather", enableFeather, default.enableFeather)
                addDefault("producer", producerName, default.producerName)
                addDefault("comments", comments, default.comments)

                // When we save, we use the current version, not the version of the loaded file.
                // This is because "load()" may be backwards compatible, but save always uses the latest format.
                add("shakyVersion", SHAKY_VERSION)
            }
        }
    }

    // endregion gameInfo

    // region ==== Folders ====

    fun saveFolders(parent: ParentResource): JsonArray {
        return JsonArray().apply {
            for (folder in parent.children.values.filterIsInstance<FolderResource>().sortedBy { it.name }) {
                add(JsonObject().apply {
                    with(folder) {
                        add("name", name)
                    }
                    saveParent(folder)
                })
            }
        }
    }
    // endregion

    // region ==== Inputs ====

    fun saveInputs(parent: ParentResource): JsonArray {
        return JsonArray().apply {
            for (inputResource in parent.children.values.filterIsInstance<InputResource>().sortedBy { it.name }) {
                add(JsonObject().apply {
                    with(inputResource) {
                        add("name", name)
                        val jItems = JsonArray()
                        for (input in inputResource.inputs) {

                            when (input) {

                                is KeyInput -> {
                                    jItems.add(JsonObject().apply {
                                        with(input) {
                                            add("type", "KeyInput")
                                            add("key", key.name)

                                        }
                                    })
                                }

                                is MouseInput -> {
                                    jItems.add(JsonObject().apply {
                                        with(input) {
                                            add("type", "MouseInput")
                                            add("button", buttonIndex)
                                        }
                                    })
                                }
                            }

                            if (input is ModifiersInput) {
                                with(input) {
                                    addDefault("checkModifiers", checkModifiers, false)
                                    if (checkModifiers) {
                                        addDefault("control", controlRequired, false)
                                        addDefault("shift", shiftRequired, false)
                                        addDefault("alt", altRequired, false)
                                        addDefault("super", superRequired, false)
                                    }
                                }
                            }
                        }
                        addArray("items", jItems)
                    }
                })
            }
        }
    }
    // endregion

    // region ==== Textures ====
    fun saveTextures(parent: ParentResource): JsonArray {
        return JsonArray().apply {
            for (textureResource in parent.children.values.filterIsInstance<TextureResource>().sortedBy { it.name }) {
                add(JsonObject().apply {
                    add("name", textureResource.name)
                    add("type", textureResource.textureType.name)

                    when (textureResource) {
                        is GridTextureResource -> {
                            val default = GridTextureResource(textureResource.parentResource)
                            with(textureResource) {
                                addDefault("xPadding", xPadding, default.xPadding)
                                addDefault("yPadding", xPadding, default.yPadding)
                                addDefault("xSpacing", xSpacing, default.xSpacing)
                                addDefault("ySpacing", ySpacing, default.ySpacing)
                                add("poseWidth", poseWidth)
                                add("poseHeight", poseHeight)

                                add("sharedOffsets", sharedOffsets)
                                if (sharedOffsets) {
                                    add("sharedXOffset", sharedXOffset)
                                    add("sharedYOffset", sharedYOffset)
                                }
                            }
                        }
                    }

                    // Poses
                    when (textureResource) {
                        is GridTextureResource -> addArray("gridPoses", saveGridPoses(textureResource))
                        is ScatteredTextureResource -> addArray("poses", saveScatteredPoses(textureResource))
                        is SingleTextureResource -> {
                            add("xOffset", textureResource.xOffset)
                            add("yOffset", textureResource.yOffset)
                        }
                    }
                })
            }
        }
    }

    fun saveGridPoses(grid: GridTextureResource): JsonArray {
        return JsonArray().apply {
            for (y in 0 until grid.down) {
                for (x in 0 until grid.across) {
                    val pose = grid.findPose(x, y)
                    if (pose != null) {
                        add(JsonObject().apply {
                            add("x", x)
                            add("y", y)
                            addDefault("name", pose.name, "")
                            if (! grid.sharedOffsets) {
                                add("xOffset", pose.xOffset)
                                add("yOffset", pose.yOffset)
                            }
                        })
                    }
                }
            }
        }
    }

    fun saveScatteredPoses(textureResource: ScatteredTextureResource): JsonArray {
        return JsonArray().apply {
            for (pose in textureResource.children.values.filterIsInstance<ScatteredPose>().sorted()) {
                add(JsonObject().apply {
                    with(pose) {
                        add("name", name)
                        add("left", left)
                        add("top", top)
                        add("width", width)
                        add("height", height)
                        add("xOffset", pose.xOffset)
                        add("yOffset", pose.yOffset)
                    }
                })
            }
        }
    }

    // endregion Textures

    // region ==== Helper Functions ====

    private fun JsonObject.addArray(name: String, value: JsonArray) {
        if (! value.isEmpty) {
            add(name, value)
        }
    }

    private fun JsonObject.addDefault(name: String, value: String, defaultValue: String) {
        if (value != defaultValue) {
            add(name, value)
        }
    }

    private fun JsonObject.addDefault(name: String, value: Boolean, defaultValue: Boolean) {
        if (value != defaultValue) {
            add(name, value)
        }
    }

    private fun JsonObject.addDefault(name: String, value: Int, defaultValue: Int) {
        if (value != defaultValue) {
            add(name, value)
        }
    }

    private fun JsonObject.addDefault(name: String, value: Float, defaultValue: Float) {
        if (value != defaultValue) {
            add(name, value)
        }
    }

    // endregion Helper Functions
}
