package uk.co.nickthecoder.shaky.core.resources

import uk.co.nickthecoder.glok.property.boilerplate.ImageTernaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.ObservableImage
import uk.co.nickthecoder.glok.property.boilerplate.intProperty

class ScatteredPose(

    override val textureResource: TextureResource

) : AbstractResource(textureResource), Pose {

    override val topProperty by intProperty(0)
    override var top by topProperty

    override val leftProperty by intProperty(0)
    override var left by leftProperty

    override val widthProperty by intProperty(1)
    override var width by widthProperty

    override val heightProperty by intProperty(1)
    override var height by heightProperty

    override val xOffsetProperty by intProperty(0)
    override var xOffset by xOffsetProperty

    override val yOffsetProperty by intProperty(0)
    override var yOffset by yOffsetProperty

    override val imageProperty: ObservableImage =
        ImageTernaryFunction(textureResource.textureProperty, leftProperty, topProperty) { texture, left, top ->
            texture.partialImage(
                left.toFloat(), top.toFloat(),
                width.toFloat(), height.toFloat()
            )
        }.apply {
            addDependent(widthProperty)
            addDependent(heightProperty)
        }
    override val image by imageProperty

}
