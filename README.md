# Shaky

A 2D game engine which aims to be easy to use, requiring very little application code,
and no boilerplate.
Shaky is primarily spite-based. You won't use shaders, or other complexities found in
more feature-rich game engines.

This is a complete re-write of my previous game engine
[Tickle](http://nickthecoder/software/tickle).

## Status

Early stages of development. Move along :-)

## Why start from scratch?

Tickle has three design flaws, which are tricky to fix in an incremental fashion :

1. I used JavaFX as the toolkit for the editor.
   This was a PITA in many ways. I'm now using [Glok](http://nickthecoder.co.uk/software/glok).
2. Tickle's resources aren't hierarchical. This is good for small games, but annoying when games get larger.
3. Tickle uses [ParaTask](http://nickthecoder.co.uk/softtware/paratask) for much of the GUI.
   I'm now using bound `Glok Properties` to coordinate the GUI and the model. So much better.
   This wasn't possible with JavaFX (unless the games runtime had the whole of JavaFX as a dependency)

It would have taken longer to fix them than starting from scratch.
