package uk.co.nickthecoder.shaky

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.ApplicationStatus
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.application.Restart
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.control.ContentDisplay
import uk.co.nickthecoder.glok.control.ExtensionFilter
import uk.co.nickthecoder.glok.control.FileDialog
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.property.boilerplate.OptionalNodeBinaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.SimpleBooleanProperty
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.styles.TINTED
import uk.co.nickthecoder.shaky.core.resources.Resources
import uk.co.nickthecoder.shaky.core.resources.loadGameInfo
import uk.co.nickthecoder.shaky.editor.Editor
import java.io.File

/**
 * A GUI showing all installed games, with a thumbnail for each game.
 * Click to play the game.
 *
 * In [developerMode][developerModeProperty], each game also has an _edit_,
 * which launches Shaky's built-in [Editor].
 */
class Launcher : Application() {

    private val sortByNameProperty = SimpleBooleanProperty(false)
    private val developerModeProperty = SimpleBooleanProperty(true)

    override fun start(primaryStage: Stage) {

        val mainContentProperty = OptionalNodeBinaryFunction(
            Shaky.recentShakyFiles.fileCount, sortByNameProperty
        ) { _, sortByName -> launcherContents(primaryStage, sortByName) }

        with(primaryStage) {
            scene(800, 600) {
                theme(LauncherTheme.themeProperty)
                title = "Shaky Launcher"
                root = borderPane {
                    top = threeRow {
                        style(".title")

                        center = label("Shaky")
                        right = menuButton("Options") {
                            style(TINTED)
                            style(".no_arrow")
                            contentDisplay = ContentDisplay.GRAPHIC_ONLY
                            graphic = Tantalum.icon("hamburger")

                            radioChoices(sortByNameProperty) {
                                + propertyRadioMenuItem(true, "Sort by Name")
                                + propertyRadioMenuItem(false, "Sort by Most Recent")
                            }
                            + Separator()
                            + toggleMenuItem("Dark Theme") {
                                selectedProperty.bidirectionalBind(Tantalum.darkProperty)
                            }
                            + toggleMenuItem("Developer Mode") {
                                selectedProperty.bidirectionalBind(developerModeProperty)
                            }
                            + Separator()

                            + menuItem("New Game Wizard") {
                                visibleProperty.bindTo(developerModeProperty)
                                onAction { newGameWizard() }
                            }
                            + menuItem("Add Game") {
                                visibleProperty.bindTo(developerModeProperty)
                                onAction { addGame(primaryStage) }
                            }
                        }
                    }
                    center = scrollPane {
                        // TODO Replace with a TabPane, with "Installed Games", and "Game Store" tabs.
                        content = singleContainer(mainContentProperty)
                    }
                    bottom = hBox {
                        padding(6)

                        + spacer()
                        + button("Exit") {
                            overrideMinWidth = 100f
                            cancelButton = true
                            onAction { scene?.stage?.close() }
                        }
                    }
                }
            }
            show()
        }
    }


    private fun launcherContents(primaryStage: Stage, sortByName: Boolean) = flowPane {
        padding(16)
        xSpacing = 10f
        ySpacing = 10f

        val files = Shaky.recentShakyFiles.withExtension("shaky")
        val sortedFiles = if (sortByName) files.sorted() else files

        for (shakyFile in sortedFiles) {
            val directory = shakyFile.parentFile
            val gameInfo = loadGameInfo(shakyFile)
            val resources = backend.resources(directory.path)
            val thumbnail = try {
                resources.loadTexture("thumbnail.png")
            } catch (e: Exception) {
                null
            }
            + button("") {
                style(".gameButton")
                graphic = vBox {
                    alignment = Alignment.CENTER_CENTER
                    + imageView(thumbnail)
                    + hBox {
                        alignment = Alignment.CENTER_CENTER
                        spacing(4)
                        + label(gameInfo.title)
                        + button("E") {
                            style(".editButton")
                            style(TINTED)
                            visibleProperty.bindTo(developerModeProperty)
                            tooltip = TextTooltip("Edit the game")
                            onAction { editGame(primaryStage, shakyFile) }
                        }
                    }
                }
                onAction { playGame(primaryStage, shakyFile) }
            }
        }
    }

    private fun editGame(primaryStage: Stage, shakyFile: File) {
        GlokSettings.restarts.add(Restart(Editor::class, arrayOf(shakyFile.path)))
        Application.instance.status = ApplicationStatus.REQUEST_QUIT
    }

    private fun playGame(primaryStage: Stage, shakyFile: File) {
        Shaky.recentShakyFiles.remember(shakyFile)
        println("Play not implemented yet")
    }


    private fun newGameWizard() {
        println("New game wizard not implemented yet")
    }

    private fun addGame(primaryStage: Stage) {
        FileDialog().apply {
            title = "Add Game"
            extensions.add(ExtensionFilter("Shaky File", "*.shaky"))
            showOpenDialog(primaryStage) { file ->
                if (file != null) {
                    Shaky.recentShakyFiles.remember(file)
                }
            }
        }
    }

}
