package uk.co.nickthecoder.shaky

import uk.co.nickthecoder.glok.application.doubleDashArguments
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.shaky.editor.Editor
import java.io.File
import java.util.prefs.Preferences
import kotlin.system.exitProcess

/**
 * The main entry point for the Shaky game engine.
 * See [USAGE] for command line options.
 *
 * If no arguments are supplied, then the [Launcher] application is shown.
 * If a _.shaky_ file is supplied, then the game is started without going through the [Launcher].
 * You may also use the _--edit__ option with a _.shaky_ file to start the [Editor].
 */
object Shaky {

    val recentShakyFiles = RecentFiles(Preferences.userNodeForPackage(Shaky::class.java))

    @JvmStatic
    fun main(vararg argv: String) {
        val arguments = doubleDashArguments(
            listOf('h', 'e'),
            listOf("help", "edit"),
            emptyList(),
            argv
        )
        if (arguments.flag('h', "help")) {
            println(USAGE)
            exitProcess(0)
        }
        if (arguments.remainder().size > 1) {
            println("Unexpected arguments : ${arguments.remainder()}")
            exitProcess(2)
        } else if (arguments.remainder().isEmpty()) {
            launch(Launcher::class)
        } else {
            val shakyFile = File(arguments.remainder().first())
            if (! shakyFile.exists()) {
                println("Shaky file not found : $shakyFile")
                exitProcess(3)

            } else {
                recentShakyFiles.remember(shakyFile)
                if (arguments.flag('e', "edit")) {
                    launch(Editor::class, arrayOf(shakyFile.path))
                } else {
                    // launchGame(shakyFile)
                }
            }
        }
    }

    val USAGE = """
        Shaky Usage
        ===========
        
        The Launcher (no arguments)
            shaky
            
        Play a game
            shaky [PLAY_OPTIONS] SHAKY_FILE
        
        Edit a game
            shaky --edit [EDIT_OPTIONS] SHAKY_FILE
            
        PLAY_OPTIONS
            None yet
        
        EDIT_OPTIONS
            None yet
        """.trimIndent()
}
