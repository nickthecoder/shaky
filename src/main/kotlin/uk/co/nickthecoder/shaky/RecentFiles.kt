package uk.co.nickthecoder.shaky

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.collections.sizeProperty
import uk.co.nickthecoder.glok.property.boilerplate.ObservableInt
import java.io.File
import java.util.prefs.Preferences


/**
 * Keeps track of recently opened files, saving them as Java Preferences.
 */
class RecentFiles(
    private val preferences: Preferences,
    private val maxItems: Int = 20,
    private val maxItemsByExtension: Int = maxItems
) {

    private val files = mutableListOf<File>().asMutableObservableList()
    val fileCount: ObservableInt = files.sizeProperty()

    val existingFiles: List<File> get() = files.filter { it.exists() }

    init {
        preferences.keys().sortedBy { it.toIntOrNull() ?: 1000 }.forEachIndexed { i, child ->
            if (i <= maxItems) {
                preferences.get(child, null)?.let { value ->
                    val file = File(value)
                    if (file.exists() && file.isFile) {
                        files.remove(file)
                        files.add(file)
                    }
                }
            }
        }
    }

    fun withExtension(extension: String) = files.filter { it.exists() && it.extension == extension }

    fun remember(file: File) {
        val abs = file.absoluteFile
        files.remove(abs)
        files.add(0, abs)
        if (files.size > maxItems) {
            files.removeAt(maxItems)
        } else if (maxItemsByExtension < maxItems) {
            val byType = withExtension(file.extension)
            if (byType.size > maxItemsByExtension) {
                files.remove(byType.last())
            }
        }

        preferences.clear()
        files.forEachIndexed { i, f ->
            preferences.put(i.toString(), f.path)
        }
        preferences.flush()
    }

}
