package uk.co.nickthecoder.shaky

import uk.co.nickthecoder.glok.control.ContentDisplay
import uk.co.nickthecoder.glok.property.boilerplate.ObservableTheme
import uk.co.nickthecoder.glok.property.boilerplate.ThemeUnaryFunction
import uk.co.nickthecoder.glok.text.Font
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.dsl.theme
import uk.co.nickthecoder.glok.theme.styles.GRAPHIC
import uk.co.nickthecoder.glok.theme.styles.IMAGE_VIEW
import uk.co.nickthecoder.glok.theme.styles.LABEL

object LauncherTheme {

    val themeProperty: ObservableTheme = ThemeUnaryFunction(Tantalum.themeProperty) {
        it.combineWith(buildTheme())
    }

    private fun buildTheme() = theme {
        ".title" {
            padding(12)
            plainBackground(Tantalum.background2Color)
            borderSize(0, 0, 1, 0)
            plainBorder(Tantalum.strokeColor)

            child(LABEL) {
                font(Font.basedOn(Tantalum.font, size = 30f))
            }
        }
        ".gameButton" {
            contentDisplay(ContentDisplay.GRAPHIC_ONLY)
            prefWidth(200)
            child(GRAPHIC) {
                spacing(4)
                child(IMAGE_VIEW) {
                    prefWidth(150)
                    prefHeight(100)
                }
                descendant(".editButton") {
                    //
                }
            }
        }
    }
}
